\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{Definitions}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}



\newcommand{\Le}[1]{{\color{red}{\bf\sf [note: #1]}}}
\newcommand{\Santosh}[1]{{\color{pink}{\bf\sf [note: #1]}}}
\newcommand{\Bo}[1]{{\color{blue}{\bf\sf [note: #1]}}}

\title{Learn Convex}


\begin{document} 

\maketitle

\section{Formulation}
We want to learn a function
\[
h(o) = \sum_{i=1}^N \max\rbr{x_i^\top o, 0}
\]
by minimizing the following non-convex optimization problem
\begin{align*}
\min_{\cbr{x_i}}\frac{1}{2} \sum_{j=1}^M \rbr{h(o_j) - r_j}^2 + \frac{\lambda}{2}\sum_{i=1}^N \nbr{x_i}_2^2 
\end{align*}

With a change of variables, we can reformulate it to an equivalent problem
\begin{align*}
\min_{\cbr{w \in \Wcal}} &\; \frac{1}{2} \nbr{w- r}^2 + \frac{\lambda}{2}\sum_{i=1}^N \nbr{x_i}_2^2  \\
\min_{\cbr{w_i \in \Wcal_i}} &\; \frac{1}{2} \nbr{\sum_{i=1}^N w_i - r}^2 + \frac{\lambda}{2}\sum_{i=1}^N \nbr{x_i}_2^2 
\end{align*}


\Bo{There is some problem with the regularization. Is it a convex function of $w$? For now, we just ignore the regularizer.}
where $w_i \in \RR^M$ and $w = w_1 + \cdots + w_N$.

The set $\Wcal_i$ is defined as
\begin{align}
\Wcal_i = \cbr{w \mid w = \Pi\sbr{O^\top x_i}, x_i \in \RR^d }
\end{align}
where $O \in \RR^{d \times M}$ is the data matrix and $\Pi\sbr{x}$ is the projection of a vector $x$ to the positive orthant. That is,
\[
\Pi\sbr{x} = \argmin_{z \in \RR_{+}^M} \nbr{z - x}
\]

The set $\Wcal = \Wcal_1 + \cdots + \Wcal_N$ is the Minkowski sum of the individual sets, i.e., $\Wcal = \cbr{w_1+\cdots+w_N \mid w_i \in \Wcal_i}$.


\subsection{Properties of $\Wcal$}
\begin{proposition}
All sets $\Wcal_i$ are the same.
\end{proposition}
This is obviously true from the definition.

\begin{proposition}
$\Wcal_1$ contains rays, i.e., if $w \in \Wcal_1$, then for any $\alpha \ge 0$, $\alpha w \in \Wcal_1$.
\end{proposition}
\begin{proof}
First we prove a property of projection onto the positive orthant. That is, $\alpha \Pi\sbr{x} = \Pi\sbr{\alpha x}$ for any $\alpha \ge 0$.
For $\alpha = 0$, it obviously holds. For $\alpha > 0$, by definition, $\Pi\sbr{x}$ is the solution to
\[
 \min_{y \in \RR_+}\nbr{y - x}
 \]
 So, $\alpha \Pi\sbr{x}$ is the solution to
 \[
 \min_{y^\prime \in \RR_+}\nbr{y\prime - \alpha x}
 \]
 which by definition implies $\alpha \Pi\sbr{x} = \Pi\sbr{\alpha x}$.
 
Suppose $w = \Pi\sbr{O^\top x} \in \Wcal_1$, then for any $\alpha \ge 0$, we have $\alpha w = \alpha\Pi\sbr{O^\top x} =  \Pi\sbr{O^\top \rbr{\alpha x}} \in \Wcal_1$.
\end{proof}

\begin{proposition}
$\Wcal_1 + \Wcal_1 = \text{conv}\rbr{\Wcal_1}$.
\label{prop:sum2cvx}
\end{proposition}
\begin{proof}
Now, we want to prove a point in $\text{conv}\rbr{\Wcal_1}$ is also a point in $\Wcal_1 + \Wcal_1$. Suppose there are two points $w_1  \in \Wcal_1$ and $w_2 \in \Wcal_1$. The point $w = \lambda w_1 + (1-\lambda) w_2$ for $0 \le \lambda \le 1$ is in the convex hull of $\Wcal_1$. At the same time, $\lambda w_1  \in \Wcal_1$ and $(1-\lambda) w_2 \in \Wcal_1$. By definition, $w \in \Wcal_1 + \Wcal_1$.

The reverse direction: $w = w_1 + w_2$ is in the Minkowski sum. Pick any $0 < \lambda < 1$, we have $w = \lambda \frac{w_1}{\lambda} + (1-\lambda) \frac{w_2}{1-\lambda}$. Since, $ \frac{w_1}{\lambda}$ and $\frac{w_2}{1-\lambda}$ are both in $\Wcal_1$, $w$ lies on a line segment between two points in $\Wcal_1$, thus $w$ lies in the convex hull of $\Wcal_1$.
\end{proof}


\begin{proposition}
$\Wcal = \text{conv}\rbr{\Wcal_1}$.
\end{proposition}
\begin{proof}
We first show that $\text{conv}\rbr{\Wcal_1}+\Wcal_1 = \text{conv}\rbr{\Wcal_1}$.
Suppose $w_1, w_2, w_3 \in \Wcal_1$, and $w = \sbr{\alpha w_1 + (1-\alpha) w_2} + w_3$ where $0 \le \alpha \le 1$.

Pick any $0 < \lambda < 1$, we have
\begin{align*}
w &=\lambda \frac{\sbr{\alpha w_1 + (1-\alpha) w_2}}{\lambda} + (1-\lambda)\frac{w_3}{1-\lambda} \\
&= \lambda\alpha \frac{w_1}{\lambda} + \lambda (1-\alpha)\frac{w_2}{\lambda} + (1-\lambda)\frac{w_3}{1-\lambda}
\end{align*}

Since $ \frac{w_1}{\lambda}, \frac{w_2}{\lambda}, \frac{w_3}{1-\lambda} \in \Wcal_1$, so $w \in \text{conv}\rbr{\Wcal_1}$.

The reverse direction: $w = \lambda w_1 + (1-\lambda) w_2 \in  \text{conv}\rbr{\Wcal_1}$, since $\lambda w_1 \in  \text{conv}\rbr{\Wcal_1}$ and $ (1-\lambda) w_2  \in \Wcal_1$, $w$ is in the Minskowski sum.


So $\Wcal = \Wcal_1 + \cdots + \Wcal_N = \Wcal_1 + \cdots + \Wcal_1$ = $\text{conv}\rbr{\Wcal_1} + \cdots  = \text{conv}\rbr{\Wcal_1}$ by using Proposition~\ref{prop:sum2cvx} and the above result.
\end{proof}



\subsection{Characterizing $\Wcal_1$}
$\Wcal_1$ is the subspace $S_O = \cbr{y \mid y = O^\top x, x \in \RR^d}$ projected to the positive orthant. So, it consists of part of the subspace that actually lies inside the positive orthant (maybe an empty set) and all the non-negative axes that have points projected onto them.

Let us denote $\Pcal_0$ the part of the subspace that lies inside the positive orthant. It is simply the intersection of $S_O$ and the positive orthant
\[
\Pcal_0 = S_O \cap \cbr{w \mid e_i^\top w \ge 0, i = 1,\dots,M}
\]
where $e_i$ is the unit vector along the $i$-th axis.

We can also characterizing $\Pcal_0$ by $M-d$ equalities (null space of $O$) and $M$ inequalities
\[
\Pcal_0 = \cbr{w \mid u_j^\top w = 0, j=1,\dots,M-d, e_i^\top w \ge 0, i = 1,\dots,M}
\]
where $\cbr{u_j}$ is a basis in the null space of $O^\top$.


Denote the non-negative axes
\[
\Pcal_i = \cbr{w \mid w = \alpha e_i, \alpha \ge 0}
\]

The $\Pcal_i$'s that are contained in $\Wcal_1$ have points projected onto them from $S_O$. To find which $\Pcal_i$'s contain projections, we first consider the set that projects to $e_i$
\[
\Ical_i = \cbr{w \mid e_i^\top (w - e_i)  = 0, e_j^\top w \le 0, \forall j \neq i}
\]
which just means points with the $i$-th element being 1 and all other elements non-positive.

We then intersect $\Ical_i$ with $S_O$. If the intersection is non-empty, it implies there exist points from $S_O$ that are projected onto $e_i$, thus $\Pcal_i$. Therefore, we can use the indicator variable
\[
\delta_i = \II\sbr{\abr{\Ical_i \cap S_O}>0}
\]
to denote whether $\Pcal_i$ is contained in $\Wcal_1$. Algorithmically, it entails solving a linear programming.


\subsection{Representing $\Wcal$ as Minkowski sum}
Given these pieces $\Pcal_i, i=0, \dots, M$, we want to prove $\Wcal = \Pcal_0 + \cdots + \Pcal_M$.

$\Wcal$ is a polyhedron \Bo{how to prove this rigoriously?}.




In summary, the problem is (without identifying $x_i$'s)
\[
\min_{v_i\in \Pcal_i}  \frac{1}{2} \nbr{\sum_{i=0}^M \delta_i v_i - r}^2
\]
where $\delta_0=1$, and $\delta_i=1$ if $e_i$ has points from the subspace projected onto it, otherwise $\delta_i = 0$.


\section{Analysis}
Our algorithm finds a solution
\[
\check{x} \in \argmin L(x, \mu^*),
\]
where $\mu^*$ maximizes the dual problem.

We want to prove
\[
\tilde{\nabla}_{\mu} L(\check{x}, \mu^*) \approx 0
\]

\begin{theorem}
Suppose there exists an optimal dual multiplier $\mu^*$ to the dual problem. Then there exists a solution $\check{x}$ to the dual problem such that
\[
\nbr{\mu^* - \sum_{i=1}^N g_i(\check{x}_i)} \le M \delta_g
\]
\end{theorem}
\begin{proof}
Define $\check{\Xcal}^*$ to be the set of optimal solutions to the dual problem by
\[
\check{\Xcal}^* = \argmin_{x\in \Xcal} \rbr{\mu^*}^\top \rbr{\sum_{i=1}^N g_i(x_i)} - \frac{1}{2}\nbr{\mu^*}_2^2 + \sum_{i=1}^N \nbr{x_i}_2^2
\]
Since it is decomposable wrt to $x_1, \cdots, x_N$, we can write $\check{\Xcal} = \Pi_{i=1}^N \check{\Xcal}_i^*$, where $\check{\Xcal}^*_i$ is for each $x_i$.
\end{proof}

Since the dual problem is concave and $\mu^*$ is a maximizer, we have
\[
0 \in \partial Q(\mu^*) = \text{conv}\cbr{ \sum_{i=1}^N g_i(x_i) - \mu^*\mid  x \in \check{\Xcal}^*}
\]

Then we have
\begin{align*}
\mu^* &\in \text{conv} \cbr{\sum_{i=1}^N g_i(x_i) \mid x_i \in \check{\Xcal}^*_i, i = 1,\cdots, N} \\
&=  \text{conv} \cbr{\Gcal_1 + \cdots + \Gcal_N}
\end{align*}

By using Shapley-Folkman Lemma, there exists $y_1, \cdots, y_N$ such that
\[
\mu^* = y_1 + \cdots + y_N
\]
where among $M$ out of them satisfy $y_i \in \text{conv}(\Gcal_i)/G_i$ and the rest satisfy $y_i \in \Gcal_i$.

For those $y_i \in \Gcal_i$, we can set them to be $y_i = g_i(\check{x}_i)$ and it corresponds to zero in the corresponding component of the gradient.

For those $y_i \in \text{conv}(\Gcal_i)/G_i$, we can set $\check{x}_i$ that is closest to $y_i$, thus distance is bounded by non-convexity parameter of the set $\Gcal_i$.

So, after all, we have
\begin{align}
\nbr{\mu^* - \sum_{i=1}^N g_i(\check{x}_i)} &= \nbr{y_1 + \cdots + y_N - \sum_{i=1}^N g_i(\check{x}_i)} \\
& \le \nbr{y_1 - g_1(\check{x}_1)} + \cdots + \nbr{y_N - g_1(\check{x}_N)} \\
&\le M \delta_g
\end{align}

%%%%%%%%%%%%%%%%%%%% Reference %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliographystyle{unsrt}
% \bibliography{../../../../bibfile2013/bibfile}
\bibliography{../../../bibfile/bibfile}

\end{document} 
