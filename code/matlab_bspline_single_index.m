function [func_bspline, v, ydiff] = matlab_bspline_single_index(x, y, options)
% function [func_bspline, v, xproj, yest] = matlab_bspline_single_index(x, y, options)

% INPUT
% param x, y: data points to be interpolated
%             x is n-by-d vector
%             y is n-by-1 vector
% param options: options parameter for the models
%                options.k -- order of B-Spline
%                options.lambda -- regularization for B-spline matrix
%                options.nbasis -- number of basis of B-Spline
%                options.rescale -- rescale the range of projected x for
%                bspline

%                optimization options for out loop
%                options.iter -- number of iterations


% param options.vture: for debug

% OUTPUT
% param alpha: weights for each B-spline basis
% param v: projection direction

[n, d] = size(x);
tol = 1e-4;

% random initialize the projection direction
v = randn(d, 1);

ydiff = zeros(options.iter, 1);

for i = 1: options.iter
     
    % update h
    xproj = x * v;
    
    func_bspline = spap2(options.nbasis, options.k, xproj, y);
    yest = fnval(func_bspline, xproj);

    if options.test
        hold off
        plot(xproj, y, 'bx');
        hold on;
        plot(xproj, yest, 'rx');
        drawnow
    end
    
    ydiff(i) = mean((yest - y).^2) / 2;
    
    if mod(i, options.interval) == 0
        fprintf('--iter %i/%i error %.4f\n', i, options.iter, ydiff(i));
    end
    if i>1 && (ydiff(i-1) - ydiff(i))/ydiff(i-1) < tol
        fprintf('--converged\n')
        return
    end
    
    % update v
    % stepsize = 1;
    if options.gradient == 1
        v = (1 - options.stepsize*options.reg) * v +  options.stepsize / n * x' * (y - yest);
    else 
        grad_bspline = fnder(func_bspline, 1);
        gtemp = fnval(grad_bspline, xproj);
        
        v = (1 - options.stepsize*options.reg) * v +  options.stepsize / n * x' * ((y - yest) .* gtemp);
    end
end