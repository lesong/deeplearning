%% test bspline nonlinear
clear; clc
x = randn(100, 1);
y = x.^2;

[xtemp, idx] = sort(x);
plot(xtemp,y(idx), 'r')
plot(xtemp,y(idx), 'm*')
hold on

gap = max(x) - min(x);
intv = 1.1*min(x): gap / length(x): 1.1*max(x);

%%
k = 3;
options.lambda = 1e-8;
[weights, B] = bspline_nonlinear(k, intv, x, y, options);

ypred = B*weights;
plot(xtemp, ypred(idx), 'b')

% 
% % [C, U] = bspline_deboor(k, intv, weights');
% [dt,dP] = bspline_deriv(k, intv, weights');
% % [dC, dU] = bspline_deboor(k-1,dt,dP);
% 
% dB = bspline_basismatrix(k-1, dt, dt);
% grad_pred = dB * dP';
% 
% plot(dt, grad_pred, 'm')