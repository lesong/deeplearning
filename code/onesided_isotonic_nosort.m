function s = onesided_isotonic_nosort(x, y, l)
% Solve one-sided isotonic regression
%
%   min_s ||s - y||^2,
%   s.t., As <= lAx
%

y_ = y - l * x;
s = lsqisotonic_nosort(x, y_);
s = s + l * x;