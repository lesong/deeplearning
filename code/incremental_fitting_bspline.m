
function [weights, basis_funcs, vproj, residual, test_error] = incremental_fitting_bspline(x, y, xtest, ytest, options)

% INPUT
% param x, y: data points to be interpolated
%             x is n-by-d vector
%             y is n-by-1 vector
% param options: parameter for the models
%                options.nfunc -- total number of functions
%                options.k -- order of B-Spline
%                options.lambda -- regularization for B-spline matrix
%                options.nbasis -- number of basis of B-Spline
%                optimization options for training basis function
%                options.iter -- number of iterations

[n, d] = size(x);
[ntest, d] = size(xtest);

weights = zeros(options.nfunc, 1);
basis_funcs = cell(options.nfunc, 1);
basis_matrix = zeros(n, options.nfunc);
vproj = zeros(d, options.nfunc);
residual = zeros(options.nfunc, 1);

basis_test_matrix = zeros(ntest, options.nfunc);
test_error = zeros(options.nfunc, 1);

L2_grad = y;
B = [];

if options.plot
    [x1, x2] = meshgrid([-3:0.2:3], [-3:0.2:3]);
    vis_x = [x1(:), x2(:), ones(length(x1(:)), 1)];
    vis_matrix = zeros(size(vis_x, 1), options.nfunc);
end

figure; 

for i = 1: options.nfunc
    
    
    clf()
    fprintf('adding %d basis\n', i);
    % learning the new basis
    [basis_funcs{i}, vproj(:, i), ~] = matlab_bspline_single_index(x, L2_grad, options);
    % updating the weights
    xproj = x * vproj(:, i);
    basis_matrix(:, i) = fnval(basis_funcs{i}, xproj);
    % nuemrical stability issue
    B = rank_one_inverse(B, basis_matrix(:, 1:i-1), basis_matrix(:, i));
    weights(1: i) = B * (y'*basis_matrix(:, 1: i))';
    % weights(1: i) = (basis_matrix(:, 1:i)' * basis_matrix(:, 1:i) + options.lambda * eye(i)) \ (y'*basis_matrix(:, 1: i))';
    % weights(i) = 1/(i+2);
    
    % evaluate the L2 gradient and residual
    ypred = basis_matrix(:, 1: i) * weights(1:i);
    L2_grad = y - ypred;
    residual(i) = mean(L2_grad.^2/2);
    
    % evaluate testing error
    xtest_proj = xtest * vproj(:, i);
    basis_test_matrix(:, i) = fnval(basis_funcs{i}, xtest_proj);
    
    ytestpred = basis_test_matrix(:, 1:i) * weights(1:i);
    test_error(i) = mean((ytest - ytestpred).^2/2);
     
    % plot figure
    if options.plot
        vis_xproj = vis_x * vproj(:, i);
        vis_matrix(:, i) = fnval(basis_funcs{i}, vis_xproj);
        vis_y = vis_matrix(:, 1:i) * weights(1:i);
        surf(x1, x2, reshape(vis_y, [size(x1, 1), size(x2, 1)]));
        % surf(x1, x2, reshape(vis_matrix(:, i), [size(x1, 1), size(x2, 1)]));
        drawnow; 
        % pause
    end
    
    fprintf('training error %5f, test error %5f\n', residual(i), test_error(i))
end

end


function newB = rank_one_inverse(B, X, v)

if isempty(B)
    newB = 1 / (v'*v); 
else
    u1 = X' * v;
    u2 = B * u1;
    d = 1 / (v' * v - u1' * u2);
    u3 = d * u2;
    invF = B + d * u2 * u2';
    newB = [invF, -u3; -u3', d];
end
end