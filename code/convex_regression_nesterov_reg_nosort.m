function [s, obj] = convex_regression_nesterov_reg_nosort(x, y, lambda)
% Assumes x is already sorted
% using projected gradient descent to solve
%
%   min_beta 1/(2n) sum_i (y_i - sum_{j<i} beta_j)^2 
%               + lambda / (2n) sum_{i=2}^n beta_i^2
%   s.t. beta_i <= beta_{i+1}
%
%   x: n x 1
%   y: n x 1

n = length(y);
eta = 0.5;
max_iter = 1e4;
interval = 50;

x_diff = [1; diff(x)];
beta = [y(1); diff(y)./diff(x)];
% projection step
beta(2:end) = lsqisotonic_nosort(x(2:end), beta(2:end));
search_pt = beta;
prev_beta = beta;
prev_alpha = 1;

obj = [];
for iter = 1:max_iter
    % evaluate at search point
    tmp = cumsum(search_pt .* x_diff);
    tmp = tmp - y;
    
    % evaluate Delta^t Delta s
    tmp = cumsum(tmp(end:-1:1));
    grad = tmp(end:-1:1) .* x_diff / n;
    grad(2:end) = grad(2:end) + lambda / n * search_pt(2:end);  % l2 regularization
    
    % gradient step
    beta = search_pt - eta * grad;
    % projection step
    beta(2:end) = lsqisotonic_nosort(x(2:end), beta(2:end));
          
    if mod(iter, interval) == 0
        tmp = cumsum(beta .* x_diff);
        tmp = tmp - y;
        fval = (sum(tmp.^2) + lambda*sum(beta(2:end).^2)) / (2*n);
        fprintf('-- iter: %i / %i, fval: %.6f\n', iter, max_iter, fval);
        obj = [obj fval];
    end
    
    % find new search point
    alpha = (1 + sqrt(1+4*prev_alpha^2)) / 2;
    search_pt = beta + (prev_alpha - 1) / alpha * (beta - prev_beta);
    
    prev_alpha = alpha;
    prev_beta = beta;
end

% convert back to function value
s = cumsum(beta .* x_diff);