function y = piecewise_linear_transfer(x)
%   piecewise linear transfer function
%   y = x, [0, 4]
%   y = 8-x, (4, inf)
%   y = -0.5x, [-2, 0)
%   y = 7+3x, (-inf, -2)

y = zeros(size(x));
idx = 0 <= x & x <=4;
y(idx) = x(idx);
idx = x > 4;
y(idx) = 8 - x(idx);
idx = -2 <= x & x < 0;
y(idx) = -0.5*x(idx);
idx = x < -2;
y(idx) = 7 + 3*x(idx);