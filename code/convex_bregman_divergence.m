function [fval, fgrad] = convex_bregman_divergence(w, y, x, param)
% Calculate Bregman divergence for the link function
% g(theta) = max(theta, a*theta), where a = param.a
% and theta = x * w
%
% The phi is 
%   y^2/(2a) I[y<=0] + y^2/2 I[y>0]
%
% loss = phi(y) - phi(g(theta)) - <theta, y - g(theta)>
% grad = x' [g(theta) - y]
%
% input
%   w:  d x 1
%   y: n x 1, targets
%   x: n x d, input
%   param: param.a,
%

a = param.a;
theta = x * w;
gt = max(theta, a*theta);

phiy = y.^2 / 2;
phiy(y<=0) = phiy(y<=0) / a;

phig = gt.^2/2;
phig(gt<=0) = phig(gt<=0) / a;

fval = sum(phiy - phig - theta .* (y - gt));
fgrad = x' * (gt - y);
