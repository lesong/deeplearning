function [w, theta, g] = bregman_isotonic_learn(x, y, l, u)
% Learns w through marginalized gradient descent
% by minimizing
%
% loss(w, phi) = phi(y) - phi(g(x*w)) - <x*w, y - g(x*w)>
%
% where g is the piecewise linear function that matches phi
% and is obtained by isotonic regression with constraints l and u.
%
% The algorithm does gradient descent on w
% the gradient is
% grad = x' [g(x*w) - y]
% the current g is calculated by isotonic regression
% with current w.
%
%   x: n x d
%   y: n x 1
%   w: d x 1
%   theta: n x 1
%   g: n x 1
%   theta and g together define the function

n = length(y);
d = size(x, 2);
max_iter = 1e3;
interval = 500;
tol = 1e-6;
% eta = 1 / u;    % some heuristic
eta = 5; 

w = randn(d, 1);
prev_a = 1;
prev_search_pt = w;
for iter = 1:max_iter
    % perform isotonic regression
    theta = x * w;
%     g = constrained_isotonic(theta, y, l, u);
    g = lsqisotonic(theta, y); 
    grad = x' * (g - y) / n;
    
    search_pt = w - eta * grad;
    a = (1 + sqrt(4*prev_a^2+1)) / 2;
    
    w = search_pt + (prev_a-1) / a * (search_pt - prev_search_pt);
    
    gradnorm = norm(grad);
    change = eta * gradnorm / norm(w);
    if change < tol
        fprintf('--finished %i/%i, gradnorm = %.6f\n',...
            iter, max_iter, gradnorm);
        break;
    end
    prev_a = a;
    prev_search_pt = search_pt;
    
    if mod(iter, interval) == 0
        fprintf('--%i/%i, gradnorm = %.6f\n',...
            iter, max_iter, gradnorm);
    end
end
% this is a slight misnormer, but according to
% nesterov' gradient descent, the final solution
% should be search_pt defined here.
w = search_pt;

if nargout > 1
    theta0 = x * w;
%     g0 = constrained_isotonic(theta0, y, l, u);
    g0 = lsqisotonic(theta0, y); 
    out = sortrows([theta0(:) g0(:)]);
    theta = out(:, 1);
    g = out(:, 2);
end