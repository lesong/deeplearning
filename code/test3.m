% test bregman isotonic learn
close all; clear; clc

d = 100;
n = 1e3;

n_repeat = 20;

err1_n = zeros(1, n_repeat);
err2_n = zeros(1, n_repeat);
err3_n = zeros(1, n_repeat);
err4_n = zeros(1, n_repeat);
err5_n = zeros(1, n_repeat);
for n_i = 1:n_repeat

wtrue = randn(d+1, 1);
x = randn(n, d);
x = [x ones(n, 1)];
theta = x * wtrue;

y = abs(theta) + 0*randn(size(theta));

l = 0.05;
u = 10;

w1 = bregman_isotonic_learn(x, y, l, u);
theta1 = x * w1;
g1 = constrained_isotonic(theta1, y, l, u);
err1 = g1 - y;
err1_n(n_i) = mean(err1.^2);

w2 = 10 * w1;
theta2 = x * w2;
g2 = constrained_isotonic(theta2, y, l, u);
err2 = g2 - y;
err2_n(n_i) = mean(err2.^2);

w3 = perceptron_isotonic_learn(x, y, l, u);
theta3 = x * w3;
g3 = constrained_isotonic(theta3, y, l, u);
err3 = g3 - y;
err3_n(n_i) = mean(err3.^2);

w4 = perceptron_isotonic_learn2(x, y, l, u);
theta4 = x * w4;
g4 = lsqisotonic(theta4, y);
err4 = g4 - y;
err4_n(n_i) = mean(err4.^2);

w5 = perceptron_isotonic_learn3(x, y, l, u);
theta5 = x * w5;
g5 = lsqisotonic(theta5, y);
err5 = g5 - y;
err5_n(n_i) = mean(err5.^2);

end

mean(err1_n)
mean(err2_n)
mean(err3_n)
mean(err4_n)
mean(err5_n)