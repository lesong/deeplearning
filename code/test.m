% test chebyshev
close all; clear all; clc

offset = 3;
x = randn(1, 8000)' - offset;
% x = rand(1, 8000)';
x_test = 1.4 * randn(1, 8000)' - offset;
% x_test = rand(1, 8000)';
x_test = sort(x_test);

% y = 1./(1+exp(-1.2*x)) + 1e-1*randn(size(x));
% y = max(0.001*x,2*x) + 1*randn(size(x));
% y = sin(pi*x) + 1*randn(size(x));
y = abs(x+offset) + 1*randn(size(x));

l = 0.001;
u = 2;

% tic;
% yhat = constrained_isotonic_old(x, y, l, u);
% toc;

tic;
yhat = constrained_isotonic(x, y, l, u);
toc;

% sum(yhat ~= yhat2)
% keyboard

plot(x(1:10:end), y(1:10:end), '.', 'MarkerSize', 5)
hold on
% plot(x, yhat, 'rx');


% test linear interpolation
tic;
[xyord] = sortrows([x(:) yhat(:)]);
pl.knots = xyord(:, 1);
pl.values = xyord(:, 2);
pl.derivatives = diff(pl.values) ./ diff(pl.knots);
pl.n_knots = length(pl.knots);

[ylin, dylin] = apply_piecewise_linear(pl, x_test);
toc;

% fit with a spline
unif = linspace(xyord(1, 1), xyord(end, 1), 20);
sp = spap2(augknt(unif, 4), 4, xyord(:, 1), xyord(:, 2));
spgood = spap2(newknt(sp), 4, xyord(:, 1), xyord(:, 2));

fnplt(sp,'r',1.5);
fnplt(spgood,'c--',1.5);

% pc = pchip(xyord(:, 1), xyord(:, 2));
spxx = unique(sp.knots);
% spyy = fnval(sp, spxx);
spyy = apply_piecewise_linear(pl, spxx);
pc = pchip(spxx, spyy);
fnplt(pc, 'b-.', 1.5);

dpc = fnder(pc, 1);
rand_idx = randsample(length(x_test), 1);
% rand_idx = 1;
x_f = x_test(rand_idx);
y_f = fnval(pc, x_f);
la = fnval(dpc, x_f);
lb = y_f - la * x_f;
x_inter = [-1:0.2:1] + x_f;
y_inter = la * x_inter + lb;
plot(x_inter, y_inter, 'g')
plot(x_f, y_f, 'o')

spxx2 = unique(spgood.knots);
% spyy2 = fnval(spgood, spxx2);
spyy2 = apply_piecewise_linear(pl, spxx2);
pc2 = pchip(spxx2, spyy2);
fnplt(pc2, 'g-.', 1.5);


plot(x_test, ylin, 'k--')

% %%
% rand_idx = randsample(length(x_test), 1);
% x_f = x_test(rand_idx);
% y_f = ylin(rand_idx);
% la = dylin(rand_idx);
% lb = y_f - la * x_f;
% x_inter = [-1:0.2:1] + x_f;
% y_inter = la * x_inter + lb;
% plot(x_inter, y_inter, 'g')
% plot(x_f, y_f, 'o')