% clear all; close all; 

generate_data

% parameters
iterno = 50;
inner_iterno = 1e4;
interval = 1000;
eta = 1e-2;
% eta = 1e-3;
tol = 1e-8;

l = 0.01;
u = 10;

% initialization
ew = zeros(iterno, 1);
wmat = zeros(d, iterno);
chebyshev = cell(iterno, 1);

train_err_mat = zeros(iterno, 1); 
test_err_mat = zeros(iterno, 1);

for iter = 1:iterno
    % Add a basis
    if iter == 1
        yy = y; 
    else
        % make current prediction
        theta = wmat(:, 1:iter-1)' * data;
        predh = zeros(size(theta));
        for i = 1:iter-1
            predh(i, :) = apply_cheby(chebyshev{i}, theta(i, :)');
        end
        predy = ew(1:iter-1)' * predh;
        yy = y - predy;
    end
    
    fprintf('*** adding basis %d ***\n', iter); 
    [w_pos, b_pos, ch_poly_pos] = learn_basis_chebyshev(data', yy', l, u);
    pos_yy = apply_cheby(ch_poly_pos, data'*w_pos+b_pos);
    pos_err = mean((yy - pos_yy').^2) / 2;
    fprintf('--error after learning basis: %.6f\n', pos_err);
    
    vis_yy = apply_cheby(ch_poly_pos, vis_data'*w_pos+b_pos);
    surf(x1, x2, reshape(vis_yy, [size(x1, 1), size(x1, 1)]));
    drawnow; 
    pause(1); 
    
    ew(iter) = 1;
    wmat(:, iter) = w_pos;
    chebyshev{iter} = ch_poly_pos;
    
    % gradient descent to fine tune
    predy_mat = zeros(iter, n_train);
    prev_yy = zeros(1, n_train);
    for inner_iter = 1:inner_iterno
        % make current prediction
        theta = wmat(:, 1:iter)' * data;
        predh = zeros(size(theta));
        gradh = zeros(size(predh));
        for i = 1:iter
            [predh(i, :), gradh(i, :)] = apply_cheby(chebyshev{i}, theta(i, :)');
        end
        predy = ew(1:iter)' * predh;
        yy = y - predy;
        
        % calculate gradient and update
        gradew = - predh * yy' / n_train;
        
%         gradwmat = zeros(d, iter);
%         for i = 1:iter
%             gradwmat(:, i) = - ew(i) * data * (yy .* gradh(i, :))' / n_train;
%         end
        
        ew(1:iter) = ew(1:iter) - eta * gradew;
%         wmat(:, 1:iter) = wmat(:, 1:iter) - eta * gradwmat;
        
        % evaluate error
        if mod(inner_iter, interval) == 0
            train_err = mean(yy.^2) / 2;
            
            % test error
            test_predh = wmat(:, 1:iter)' * test_data;
            for i = 1:iter
                test_predh(i, :) = apply_cheby(chebyshev{i}, test_predh(i, :)');
            end
            test_predy = ew(1:iter)' * test_predh;
            test_yy = test_y - test_predy;
            
            test_err = mean(test_yy.^2) / 2;
            
            fprintf('-- iter %i, inner_iter %i, train_err, %f, test_err, %f\n',...
                iter, inner_iter, train_err, test_err);
            
            % determine if converged
            if (inner_iter > 2e3)
                old_train_err = mean(prev_yy.^2) / 2;
                change = (old_train_err - train_err) / old_train_err;
                if change < tol
                    break;
                end
            end
        end
        
        prev_yy = yy;
    end
    
    train_err_mat(iter) = mean(yy.^2) / 2;
    test_err_mat(iter) = test_err / 2;
end