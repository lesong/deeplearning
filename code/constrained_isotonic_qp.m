function s = constrained_isotonic_qp(x, y, u)
% Constrained isotonic regression
%
%   min_s 0.5*||s - y||^2
%   s.t., As <= u * Ax
%

m = length(x);

[xyord, ord] = sortrows([x(:) y(:)]); iord(ord) = 1:m;
x = xyord(:, 1);
y = xyord(:, 2);

diffx = diff(x);

H = speye(m);
f = -y;
A = [ ...
  spdiags([ones(m-1,1), -ones(m-1,1)], 0:1, m-1, m); ...
  spdiags([-ones(m-1,1), ones(m-1,1)], 0:1, m-1, m) ...
  ];
A2 = spdiags([-diffx(2:end), diffx(2:end)+diffx(1:end-1), -diffx(1:end-1)], ...
  [0,1,2], m-2, m);
A = [A; A2];
b = [ ...
  zeros(m-1, 1);
  u * diffx(:); ...
  ];
b2 = zeros(m-2,1);
b = [b; b2];

%  X = quadprog(H,f,A,b) attempts to solve the quadratic programming 
%     problem:
%  
%              min 0.5*x'*H*x + f'*x   subject to:  A*x <= b 
%               x    
%  
options.Algorithm = 'interior-point-convex'; 
options.MaxIter = 2000; 
options.TolFun = 1e-10; 
options.TolCon = 1e-10; 
options.Display = 'off'; 
s1 = quadprog(H, f, A, b, [], [], [], [], y, options);

s = s1(iord); 