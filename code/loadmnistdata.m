% load mnist

rng(19240)

mnist_dir = '/nv/pcoc1/bxie33/mnist/';
addpath(mnist_dir);

traindata = loadMNISTImages([mnist_dir 'train-images-idx3-ubyte']);
trainlabel = loadMNISTLabels([mnist_dir 'train-labels-idx1-ubyte']);

testdata = loadMNISTImages([mnist_dir 't10k-images-idx3-ubyte']);
testlabel = loadMNISTLabels([mnist_dir 't10k-labels-idx1-ubyte']);

% center each image to be zero mean and unit variance
traindata = bsxfun(@minus, traindata, mean(traindata, 1));
traindata = bsxfun(@rdivide, traindata, sqrt(sum(traindata.^2, 1)));
testdata = bsxfun(@minus, testdata, mean(testdata, 1));
testdata = bsxfun(@rdivide, testdata, sqrt(sum(testdata.^2, 1)));

d = size(traindata, 1);
n_train = size(traindata, 2);
n_test = size(testdata, 2);

k = 150;
mixw = randn(k, 1);
wtrue = randn(d, k);
wtrue = bsxfun(@rdivide, wtrue, sqrt(sum(wtrue.^2, 1)));

h = max(wtrue' * traindata, 0);
train_y = mixw' * h;
h = max(wtrue' * testdata, 0);
test_y = mixw' * h;

% traindata
% train_y
% testdata
% test_y

fprintf('--finished processing data\n')