function [g, dg] = apply_pchip_pp(pchip_pp, x)
% Apply pchip function at points x, and also return the derivatives

a = pchip_pp.breaks(1);
b = pchip_pp.breaks(end);

g = ppval(pchip_pp, x);
dg = ppval(pchip_pp.deriv, x);

lx_idx = x <= a;
g(lx_idx) = pchip_pp.l_y + pchip_pp.l_deriv * (x(lx_idx) - a);
dg(lx_idx) = pchip_pp.l_deriv;

rx_idx = x >= b;
g(rx_idx) = pchip_pp.r_y + pchip_pp.r_deriv * (x(rx_idx) - b);
dg(rx_idx) = pchip_pp.r_deriv;