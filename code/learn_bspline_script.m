clear all; close all;

addpath('bspline')

generate_data3

% parameters
iterno = 50;
inner_iterno = 1e4;
interval = 1e3;
eta = 1e-2;
tol = 1e-8;
tol1 = 1e-4;

options.k = 3;
options.nbasis = 20;
options.reg =1e-1;
options.iter = 200;
options.gradient = 2;
options.interval = 4;
options.stepsize = 1;

options.test = 1;

% initialization
ew = zeros(iterno, 1);
wmat = zeros(d, iterno);
splines = cell(iterno, 1);
der_splines = cell(iterno, 1);

train_err_mat = zeros(iterno, 1); 
test_err_mat = zeros(iterno, 1);

for iter = 1:iterno
    % Add a basis
    if iter == 1
        yy = y; 
    else
        % make current prediction
        theta = wmat(:, 1:iter-1)' * data;
        predh = zeros(size(theta));
        for i = 1:iter-1
            theta(i, :) = min(max(theta(i, :), splines{i}.knots(1)), splines{i}.knots(end));
            predh(i, :) = fnval(splines{i}, theta(i, :)');
        end
        predy = ew(1:iter-1)' * predh;
        yy = y - predy;
    end
    
    fprintf('*** adding basis %d ***\n', iter);
    [curr_spline, curr_w] = matlab_bspline_single_index(data', yy', options);
    
%     % naive distribution of weights
%     ew(iter) = 1;
%     wmat(:, iter) = curr_w;
%     splines{iter} = curr_spline;
%     der_splines{iter} = fnder(curr_spline, 1);
    
    % distribute weights more evenly
    weight_norm = norm(curr_spline.coefs);
    curr_spline.coefs = curr_spline.coefs / sqrt(weight_norm);
    ew(iter) = sqrt(weight_norm);
    wmat(:, iter) = curr_w;
    splines{iter} = curr_spline;
    der_splines{iter} = fnder(curr_spline, 1);    
    
    % gradient descent to fine tune
    prev_yy = zeros(1, n_train);
    for inner_iter = 1:inner_iterno
        % make current prediction
        theta = wmat(:, 1:iter)' * data;
        predh = zeros(size(theta));
        gradh = zeros(size(predh));
        for i = 1:iter
            theta(i, :) = min(max(theta(i, :), splines{i}.knots(1)), splines{i}.knots(end));
            predh(i, :) = fnval(splines{i}, theta(i, :)');
            gradh(i, :) = fnval(der_splines{i}, theta(i, :)');
        end
        predy = ew(1:iter)' * predh;
        yy = y - predy;
        
        % calculate gradient and update
        gradew = - predh(1:iter, :) * yy' / n_train;
        
        gradwmat = zeros(d, iter);
        gradcoef = zeros(options.nbasis + options.k - 1, iter);
        for i = 1:iter
            gradwmat(:, i) = - ew(i) * data * (yy .* gradh(i, :))' / n_train;
            gradcoef(:, i) = - ew(i) * yy *...
                bspline_basismatrix(options.k, splines{i}.knots, theta(i, :)') / n_train;
        end
        
        ew(1:iter) = ew(1:iter) - eta * gradew;
        wmat(:, 1:iter) = wmat(:, 1:iter) - eta * gradwmat;
        for i = 1:iter
            splines{i}.coefs = splines{i}.coefs - eta * gradcoef(:, i)';
            der_splines{i} = fnder(splines{i}, 1);
        end
        
        % evaluate error
        if mod(inner_iter, interval) == 0
            train_err = mean(yy.^2) / 2;
            
            % test error
            test_predh = wmat(:, 1:iter)' * test_data;
            for i = 1:iter
                test_predh(i, :) = min(max(test_predh(i, :), splines{i}.knots(1)), splines{i}.knots(end));
                test_predh(i, :) = fnval(splines{i}, test_predh(i, :)');
            end
            test_predy = ew(1:iter)' * test_predh;
            test_yy = test_y - test_predy;
            
            test_err = mean(test_yy.^2) / 2;
            
            fprintf('-- iter %i, inner_iter %i, train_err, %f, test_err, %f\n',...
                iter, inner_iter, train_err, test_err);

%             fprintf('-- iter %i, inner_iter %i, train_err, %f\n',...
%                             iter, inner_iter, train_err);
            
            % determine if converged
            if (inner_iter > 2e3)
                old_train_err = mean(prev_yy.^2) / 2;
                change = (old_train_err - train_err) / old_train_err;
                if (iter < iterno && change < tol1) || (iter == iterno && change < tol)
                    break;
                end
            end
        end
        
        prev_yy = yy;
    end
    
    train_err_mat(iter) = mean(yy.^2) / 2;
    test_err_mat(iter) = test_err;
end