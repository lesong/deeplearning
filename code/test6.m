% test convex
close all; clear all; clc

[x, y] = meshgrid([-4:0.2:4], [-4:0.2:4]);
% z = sin(x) .* sin(y);
% mesh(x, y, z)
% target = z(:);

data = [x(:) y(:)];

w1 = randn(2, 1);
theta = data * w1;
% target = max(theta, -1*theta);
target = theta.^2;
% w2 = randn(2, 1);
% theta = data * w2;
% target = target + theta.^2;
z = reshape(target, size(x, 1), size(y, 1));
figure
mesh(x, y, z)

% % fit isotonic regression
% [w_iso, pwlinear_iso] = learn_basis_normalize(data, target, 0, 100);
% yy_iso = apply_piecewise_linear(pwlinear_iso, data*w_iso);
% figure
% mesh(x, y, reshape(yy_iso, size(x)));

[w_conv, pwlinear_conv] = learn_convex_basis_normalize(data, target, randn(size(w1)));
yy_conv = apply_piecewise_linear(pwlinear_conv, data*w_conv);
figure
mesh(x, y, reshape(yy_conv, size(x)));

% subspace(w1, w_iso)
subspace(w1, w_conv)
% subspace(w2, w_conv)