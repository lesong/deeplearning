% generate 2d sinual waves

[x1, x2] = meshgrid([-4:0.2:4], [-4:0.2:4]);
y0 = sin(x1) .* sin(x2);
mesh(x1, x2, y0)

data = [x1(:) x2(:)]';
y = y0(:)';

data = [data; ones(1, size(data, 2))];
[d, n_train] = size(data);
test_data = data;
test_y = y;
n_test = length(y);