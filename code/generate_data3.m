% generate data
rng(294016)

k = 150;
d = 20;
% d = 100;
noise_level = 0;

wtrue = randn(d, k);
wtrue = bsxfun(@rdivide, wtrue, sqrt(sum(wtrue.^2)));
btrue = 0.2 * randn(k, 1);
ewtrue = 5*randn(k, 1);

% [x1, x2] = meshgrid([-1:0.05:1], [-1:0.05:1]);
% vis_data = [x1(:) x2(:)]';
% n_vis = size(vis_data, 2);
% h = bsxfun(@plus, wtrue' * vis_data, btrue);
% h = max(h, 0);
% vis_y = ewtrue' * h;

% n_train = 2500;
n_train = 1e4;
% data = randn(d, n_train);
% data = rand(d, n_train) ./ sqrt(d) - 1/(2*sqrt(d));
data = rand(d, n_train) - 0.5;
h = bsxfun(@plus, wtrue' * data, btrue);
h = max(h, 0);
y = ewtrue' * h + noise_level * randn(1, n_train);

n_test = 1000;
% test_data = randn(d, n_test);
% test_data = rand(d, n_test) ./ sqrt(d) - 1/(2*sqrt(d)); 
test_data = rand(d, n_test) - 0.5; 
h = bsxfun(@plus, wtrue' * test_data, btrue);
h = max(h, 0);
test_y = ewtrue' * h + noise_level * randn(1, n_test);

% vis_data = [vis_data; ones(1, n_vis)];
data = [data; ones(1, n_train)];
test_data = [test_data; ones(1, n_test)];
d = d + 1;