function [w, pos] = sample_basis(x, y, param)

T = 500;
T = 1e3;
d = size(x, 1);

std_y = std(y);

all_w = randn(d, T);
all_w = bsxfun(@rdivide, all_w, sqrt(sum(all_w.^2)));
mixw = zeros(1, T);
best_err = inf;
for t = 1:T
    pos_pred = all_w(:, t)' * x;
    pos_pred = max(pos_pred, param.a * pos_pred);
    std_pred = std(pos_pred);
    mixw(t) = std_y / std_pred;
    pos_err = sum((y - mixw(t) * pos_pred).^2);
    if pos_err < best_err
        best_err = pos_err;
        best_w = t;
    end
end
pos_err = best_err;
w_pos = mixw(best_w) * all_w(:, best_w);

best_err = inf;
for t = 1:T
    neg_pred = all_w(:, t)' * x;
    neg_pred = max(neg_pred, param.a * neg_pred);
    std_pred = std(neg_pred);
    mixw(t) = std_y / std_pred;
    neg_err = sum((-y - mixw(t) * neg_pred).^2);
    if neg_err < best_err
        best_err = neg_err;
        best_w = t;
    end
end
neg_err = best_err;
w_neg = mixw(best_w) * all_w(:, best_w);

if pos_err < neg_err
    pos = 1;
    w = w_pos;
else
    pos = 0;
    w = w_neg;
end