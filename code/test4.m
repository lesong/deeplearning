% test chebyshev
close all; clear all; clc

offset = 0;
n = 1e2;
x = randn(1, n)' - offset;
x = sort(x);

y = 1./(1+exp(-4.6*x)) + 0e-1*randn(size(x));
% y = max(0.001*x,2*x) + 0*randn(size(x));
% y = sin(pi*x) + 1*randn(size(x));
% y = abs(x+offset) + 1*randn(size(x));
% plot(x(1:10:end), y(1:10:end), '.', 'MarkerSize', 5)
plot(x, y, '.', 'MarkerSize', 5)
hold on

% l = 0.001;
% u = 2;
% 
% tic;
% yhat1 = constrained_isotonic(x, y, l, u);
% toc;
% 
% plot(x, yhat1, 'rx');
% 
% tic;
% yhat2 = constrained_isotonic_qp(x, y, u);
% toc;
% plot(x, yhat2, 'gx')
% 
% tic;
% yhat3 = constrained_isotonic_qp2(x, y, u);
% toc;
% plot(x, yhat3, 'cx')

tic;
yhat4 = convex_isotonic_nosort(x, y);
toc;
plot(x, yhat4, 'yx')