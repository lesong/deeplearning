function s = convex_isotonic_nosort(x, y)
% Isotonic regression with convexity constraints
% solved by primal dual saddle points.
%
% Assumes y is already sorted according to x.

n = length(x);
max_iter = 100;
eta = 1e-2;
tol = 1e-5;
interval = 3;

% d_1 to d_{n-1}
d = 1./diff(x);
% lambda_1 to lambda_{n-2}
lambda = zeros(n-2, 1);
prev_lambda = lambda;
for iter = 1:max_iter
    % append l_0 = l_{n-1} = 0, then
    % calculate the difference
    % idx i = lambda_i - lambda_{i-1}
    diffl = diff([0; lambda; 0]);
    
    % append u_0 = u_n = 0
    % u_i = ( lambda_i - lambda_{i-1} ) * d_i
    u = [0; d .* diffl; 0];
    c = diff(u) / 2;
    y0 = y + c;
    
    s = lsqisotonic_nosort(x, y0);
    
    % maximize over lambda
    tmp = d .* diff(s);
    grad_lambda = - diff(tmp);
    lambda = max(prev_lambda + eta * grad_lambda, 0);
    d_lambda = lambda - prev_lambda;
    
    [~, max_idx] = max(lambda);
    max_idx
    
    if mod(iter, interval) == 0
        fprintf('-- iter %i/%i, gradnorm = %.4f\n', iter, max_iter, norm(d_lambda))
        if norm(d_lambda) < tol
            fprintf('-- finished\n')
            break
        end
    end
end