

clear; close all;

% test incremental fitting bspline

rng('default')
generate_data;

xtrain = data';
ytrain = y';

xtest = test_data';
ytest = test_y';
%%

options.nfunc = 100;

options.k = 3;
options.nbasis = 20;
options.reg =1e-1;
options.iter = 200;
% options.vtrue = randn(d, 1);
options.gradient = 2;
options.interval = 4;
options.lambda = 1e-8;
options.stepsize = 1;

options.test = false;
options.plot = true;

[weights, basis_funcs, vproj, residual, test_error] = incremental_fitting_bspline(xtrain, ytrain, xtest, ytest, options);

%%
figure
plot(residual, 'r')
hold on
plot(test_error, 'b')
