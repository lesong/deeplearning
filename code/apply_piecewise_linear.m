function [g, dg] = apply_piecewise_linear(piecewise_linear, x)
% Apply piecewise linear function at points x, and also
% return the derivatives

g = interp1(piecewise_linear.knots, piecewise_linear.values, ...
    x, 'linear', 'extrap');

[~, binx] = histc(x, piecewise_linear.knots);

binx(x <= piecewise_linear.knots(1)) = 1;
binx(x >= piecewise_linear.knots(end)) = piecewise_linear.n_knots-1;
dg = piecewise_linear.derivatives(binx);