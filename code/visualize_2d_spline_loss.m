
%% test bspline_single_index

clear; close all
clc

addpath(pwd);
randn('seed', 13)

n = 400;
d = 2;
noise_level = 0.0;

x = randn(n,d);
% x = [-4:0.02:4];
% x = repmat(x',1 ,2);

v = randn(d, 1);
v = v ./ norm(v);

% y = 1 ./ (1 + exp(-x*v));
% y = max(0, x*v);
% y = min(2, max(0, x*v));
y = (x*v).^2;
% y = max(x*v, -0.5 * x*v);
% plot(x(:, 1), x(:, 2), 'ro')
% hold on

y = y + noise_level * randn(size(y));


options.k = 3;
options.nbasis = 20;

[w1, w2] = meshgrid([-4:0.1:4], [-4:0.1:4]);
w = [w1(:) w2(:)]';
loss = zeros(1, size(w, 2));
for i = 1:size(w, 2)
    if norm(w(:, i)) == 0
        loss(i) = 0;    % undefined
    else
        xproj = x * w(:, i);
        func_bspline = spap2(options.nbasis, options.k, xproj, y);
        yest = fnval(func_bspline, xproj);
        loss(i) = sum((y - yest).^2);
    end
end

z = reshape(loss, size(w1));

mesh(w1, w2, z);