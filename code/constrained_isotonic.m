function s = constrained_isotonic(x, y, l, u)
% Constrained isotonic regression
%
%   min_s ||s - y||^2
%   s.t., uAx <= As <= lAx
%
n = length(y);
max_iter = 1e5;
interval = 1e5;
rho = 10;
tau = 2;    % multiplier to change rho
residue_gap = 10;   % threshold to change rho
tol = 1e-5 * sqrt(n);

% sort
[xyord, ord] = sortrows([x(:) y(:)]); iord(ord) = 1:n;
x = xyord(:, 1);
y = xyord(:, 2);

s1 = y;
s2 = -y;

lambda = zeros(size(s1));
prev_s2 = s2;
for iter = 1:max_iter
    % s+
    y1 = (2*y - lambda - rho*s2) / (rho + 2);
    s1 = onesided_isotonic_nosort(x, y1, l);
    
    % s-
    y2 = (-2*y - lambda - rho*s1) / (rho + 2);
    s2 = onesided_isotonic_nosort(x, y2, -u);
    
    % lambda
    lambda = lambda + rho*(s1 + s2);
    
    % optimality conditions
    dual_residue = rho * (s2 - prev_s2);
    primal_residue = s1 + s2;
    n_primal = norm(primal_residue);
    n_dual = norm(dual_residue);
    if n_primal > residue_gap * n_dual
        rho = tau * rho;
    elseif n_dual > residue_gap * n_primal
        rho = rho / tau;
    end
    if n_primal < tol && n_dual < tol
%         fprintf('--Finished. primal: %.6f, dual: %.6f\n',...
%             n_primal, n_dual)
        break
    end
    
    if mod(iter, interval) == 0
        fobj = (sum((s1 - y).^2) + sum((s2 + y).^2)) / n;
%         fprintf('--%i/%i, fobj: %.6f, primal: %.6f, dual: %.6f, rho: %.3f\n',...
%             iter, max_iter, fobj, n_primal, n_dual, rho)
    end
    prev_s2 = s2;
end

s = s1(iord);