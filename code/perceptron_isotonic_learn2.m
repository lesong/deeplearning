function [w, theta, g] = perceptron_isotonic_learn2(x, y, l, u)
% Learns w through marginalized gradient descent
% by minimizing
%
% loss(w, phi) = phi(y) - phi(g(x*w)) - <x*w, y - g(x*w)>
%
% where g is the piecewise linear function that matches phi
% and is obtained by isotonic regression with constraints l and u.
%
% The algorithm does gradient descent on w
% the gradient is
% grad = x' [g(x*w) - y]
% the current g is calculated by isotonic regression
% with current w.
%
%   x: n x d
%   y: n x 1
%   w: d x 1
%   theta: n x 1
%   g: n x 1
%   theta and g together define the function

n = length(y);
d = size(x, 2);
max_iter = 500;
interval = 100;
eta = 1; 
tol = 1e-8; 

w = randn(d, 1);
w = w ./ norm(w); 
min_err = Inf; 
min_w = w; 
prev_a = 1;
prev_search_pt = w;
old_w = w; 
for iter = 1:max_iter
    % perform isotonic regression
    theta = x * w;
%     g = constrained_isotonic(theta, y, l, u);
    g = lsqisotonic(theta, y); 

%     g = constrained_isotonic_qp(theta, y, u); 
    
    err = mean((g-y).^2);
    
    if err < min_err
      min_w = w; 
      min_err = err; 
    end    

    grad = x' * (g - y) / n;    
    search_pt = w - eta * grad;
    a = (1 + sqrt(4*prev_a^2+1)) / 2;    
    w = search_pt + (prev_a-1) / a * (search_pt - prev_search_pt);
    
    prev_a = a;
    prev_search_pt = search_pt;
    
    if mod(iter, interval) == 0
        fprintf('--%i/%i, err = %.6f, coeff=%.6f, norm change=%.6f\n',...
            iter, max_iter, min_err, (prev_a-1) / a, norm(old_w - w)/norm(old_w));
    end
    
    if (norm(old_w - w) / norm(old_w)) < tol
      break; 
    end
end
% this is a slight misnormer, but according to
% nesterov' gradient descent, the final solution
% should be search_pt defined here.
w = min_w;

if nargout > 1
    theta0 = x * w;
%     g0 = constrained_isotonic(theta0, y, l, u);
    g0 = lsqisotonic(theta0, y); 
    out = sortrows([theta0(:) g0(:)]);
    theta = out(:, 1);
    g = out(:, 2);
end