function [w, piecewise_linear, offset] = learn_basis_normalize2(x, y, l, u)
% Learn a function basis using isotonic regression and 
%   represent it as a piecewise linear function
% 
%   y = f(w'x)
%
% input:
%   x: n x d
%   y: n x 1
%
% output:
%   w: d x 1
%   piecewise_linear: a struct
%       .knots: the knots for different piecewise linear
%       .values: the corresponding values at the knots
%       .derivatives: the derivaties for each piece

if nargin <= 2
    l = 0.01;
    u = 10;
end

miny = min(y);
maxy = max(y);
scale_y = (maxy - miny) / 2;
bias_y = (maxy + miny) / 2;
y = (y - bias_y) / scale_y;     % normalize to [1, -1]

[w, theta, g] = bregman_isotonic_learn(x, y, l, u);

piecewise_linear.knots = theta;
piecewise_linear.values = scale_y * g;
piecewise_linear.derivatives = diff(g) ./ diff(theta);
piecewise_linear.n_knots = length(g);
offset = bias_y;