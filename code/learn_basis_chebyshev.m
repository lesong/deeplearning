function [w, b, ch_poly] = learn_basis_chebyshev(x, y, l, u)
% Learn a function basis using isotonic regression and then fit a 
% Chebyshev polynomial.
% 
%   y = a*f(w'x+b) + c
%
% where, a and c are scale and bias for y, such that
% f is [0, 1].
%
% input:
%   x: n x d
%   y: n x 1
%
% output:
%   w: d x 1
%   b: scalar, used to center w'x + b
%   ch_poly: a struct
%       .coeff: Chebyshev coefficients
%           which already incorporates a
%       .range: range of the interval
%       .c: bias for the function, c as above.

if nargin <= 2
    l = 0.01;
    u = 10;
end

p = 20;

% w = bregman_isotonic_learn_old(x, y, l, u); % norm(w) <= 1
w = bregman_isotonic_learn(x, y, l, u); % norm(w) <= 1
theta = x * w;
range = [min(theta) max(theta)];
ch_poly.range = 1.0 * range;

g = constrained_isotonic(theta, y, l, u);
f = @(xnew) interp1(theta, g, xnew, 'linear', 'extrap');
ch_poly.coeff = chebyshev_coefficients ( range(1), range(2), p, f );
ch_poly.c = 0;
b = 0;