% clear all; close all; 

loadmnistdata

% parameters
iterno = 50;
inner_iterno = 1e4;
sample_size = 1e4;
interval = 1e3;
eta = 1e-2;
tol = 1e-8;

% initialization
ew = zeros(iterno, 1);
wmat = zeros(d, iterno);

train_err_mat = zeros(iterno, 1); 
test_err_mat = zeros(iterno, 1);

function_values = zeros(iterno, n_train);
for iter = 1:iterno
    % Add a basis
    if iter == 1
        yy = train_y; 
    else        
        predy = ew(1:iter-1)' * function_values(1:iter-1, :);
        yy = train_y - predy;
    end
    
    fprintf('--adding a random basis\n')
    [curr_w, pos_func] = sample_basis2(traindata, yy, sample_size);
    fprintf('--finished selecting random basis\n')
    
    wmat(:, iter) = curr_w;
    function_values(iter, :) = max(curr_w' * traindata, 0);
    
    if pos_func
        ew(iter) = 1;
    else
        ew(iter) = -1;
    end
    
    % gradient descent to fine tune
    predy_mat = zeros(iter, n_train);
    prev_yy = zeros(1, n_train);
    
    for inner_iter = 1:inner_iterno
        % make current prediction
        predy = ew(1:iter)' * function_values(1:iter, :);
        yy = train_y - predy;
        
        % calculate gradient and update
        gradew = - function_values(1:iter, :) * yy' / n_train;
        
        ew(1:iter) = ew(1:iter) - eta * gradew;
        
        % evaluate error
        if mod(inner_iter, interval) == 0
            train_err = mean(yy.^2) / 2;
            
            % test error
            test_predh = wmat(:, 1:iter)' * testdata;
            test_predh = max(test_predh, 0);
            test_predy = ew(1:iter)' * test_predh;
            test_yy = test_y - test_predy;
            
            test_err = mean(test_yy.^2) / 2;
            
            fprintf('-- iter %i, inner_iter %i, train_err, %f, test_err, %f\n',...
                iter, inner_iter, train_err, test_err);
            
            % determine if converged
            if (inner_iter > 2e3)
                old_train_err = mean(prev_yy.^2) / 2;
                change = (old_train_err - train_err) / old_train_err;
                if change < tol
                    break;
                end
            end
        end
        
        prev_yy = yy;
    end
    
    train_err_mat(iter) = gather(mean(yy.^2) / 2);
    test_err_mat(iter) = gather(test_err / 2);
end