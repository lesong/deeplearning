function [w, pos] = relu_bregman_fit2d(x, y, param, winit)
if nargin >= 4
    w0 = winit;
else
    w0 = randn(size(x, 1), 1);
end

pos_myfun = @(w) convex_bregman_divergence(w, y', x', param);
opts = optimset('GradObj','on');
% opts = optimset('GradObj','on', 'DerivativeCheck', 'on');
[w_pos] = fminunc(pos_myfun, w0, opts);

pos_pred = w_pos' * x;
pos_pred = max(pos_pred, param.a * pos_pred);
pos_err = mean((y - pos_pred).^2);

w = w_pos;
pos = 1;

fprintf('--learned basis reconstruction error: %.6f\n', pos_err);