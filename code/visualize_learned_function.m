theta = wmat' * vis_data;
predh = zeros(size(theta));
for i = 1:iter
    predh(i, :) = apply_piecewise_linear(pw_linear{i}, theta(i, :)');
end
vis_yy = ew' * predh;
surf(x1, x2, reshape(vis_yy, [size(x1, 1), size(x1, 1)]));