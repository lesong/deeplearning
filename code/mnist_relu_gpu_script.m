% clear all; close all; 

loadmnistdata

gpuid = 1;
gpuDevice(gpuid)
cpu_data = traindata;
cpu_test_data = testdata;
cpu_y = train_y;
cpu_test_y = test_y;

traindata = gpuArray(cpu_data);
testdata = gpuArray(cpu_test_data);
train_y = gpuArray(cpu_y);
test_y = gpuArray(cpu_test_y);

% parameters
iterno = 50;
inner_iterno = 1e7;
interval = 1e3;
eta = 1e-2;
tol = 1e-8;

% initialization
cpu_ew = randn(iterno, 1);
cpu_wmat = randn(d, iterno);
cpu_wmat = bsxfun(@rdivide, cpu_wmat, sqrt(sum(cpu_wmat.^2, 1)));
ew = gpuArray(cpu_ew);
wmat = gpuArray(cpu_wmat);

train_err_mat = zeros(iterno, 1); 
test_err_mat = zeros(iterno, 1);

for inner_iter = 1:inner_iterno
    % make current prediction
    predh = wmat' * traindata;
    predh = max(predh, 0);
    predy = ew' * predh;
    yy = train_y - predy;

    % calculate gradient and update
    gradew = - predh * yy' / n_train;

    tmp = ew * yy;
    tmp(predh <= 0) = 0;
    gradwmat = - traindata * tmp' / n_train;

    ew = ew - eta * gradew;
    wmat = wmat - eta * gradwmat;

    % evaluate error
    if mod(inner_iter, interval) == 0
        train_err = mean(yy.^2) / 2;

        % test error
        test_predh = wmat' * testdata;
        test_predh = max(test_predh, 0);
        test_predy = ew' * test_predh;
        test_yy = test_y - test_predy;

        test_err = mean(test_yy.^2) / 2;

        fprintf('-- iter %i, train_err, %f, test_err, %f\n',...
            inner_iter, train_err, test_err);

        % determine if converged
        if (inner_iter > 2e3)
            old_train_err = mean(prev_yy.^2) / 2;
            change = (old_train_err - train_err) / old_train_err;
            if change < tol
                fprintf('reached tolerance, finishing...\n')
                break;
            end
        end
    end

    prev_yy = yy;
end