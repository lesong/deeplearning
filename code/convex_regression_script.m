close all; clear all; clc

rng(94824)

% x = [-4:0.01:4]';
% n = length(x);

n = 5e4;
% x = 4 * randn(n, 1);
x = 10 * (rand(n, 1) - 0.5);
% x = sort(x);
y = x.^2 + 1*randn(size(x));
% y = 1./(1 + exp(-x)) + 0.1* randn(size(x));
% y = sin(2*x) + 0.1* randn(size(x));

lambda = 0;

% [s_grad, obj_grad] = convex_regression_nosort(x, y);
% [s_nest, obj_nest] = convex_regression_nesterov_nosort(x, y);
% [s_nest_ls, obj_nest_ls] = convex_regression_nesterov_ls_nosort(x, y);
% [s_nest_rg, obj_nest_rg] = convex_regression_nesterov_reg_nosort(x, y, lambda);
figure
% [s_nest_lsrg, obj_nest_lsrg] = convex_regression_nesterov_ls_reg_nosort(x, y, lambda);
s = convex_regression(x, y);

figure
plot(x, y, 'gx'); hold on
% plot(x, s_grad, 'r--')
plot(x, s_nest, 'b--')
plot(x, s_nest_ls, 'c--')
plot(x, s_nest_rg, 'y--')
plot(x, s_nest_lsrg, 'k--')
% legend('data', 'grad', 'nest')

figure
% semilogy(obj_grad, 'r'); hold on
semilogy(obj_nest, 'b'); hold on
semilogy(obj_nest_ls, 'c'); hold on
semilogy(obj_nest_rg, 'y'); hold on
semilogy(obj_nest_lsrg, 'k'); hold on
% legend('grad', 'nest')