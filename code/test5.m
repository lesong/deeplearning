% test convex
close all; clear all; clc

x = -4:0.1:4;
n = length(x);
u = 10;
% y = 1./(1+exp(-x)) + 1 * randn(size(x));
y = sin(x) + 0 * randn(size(x));
s = constrained_isotonic_qp(x, y, u);
s2 = constrained_convex_qp(x, y);

plot(x, y, 'x')
hold on
plot(x, s, 'rx')
plot(x, s2, 'gx')