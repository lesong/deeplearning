
%% test bspline_single_index

clear; close all
clc

addpath(pwd);
randn('seed', 10)

n = 1e5;
d = 300;
noise_level = 0.1;

x = randn(n,d);
% x = [-4:0.02:4];
% x = repmat(x',1 ,2);

v = randn(d, 1);
v = v ./ norm(v);

% y = 1 ./ (1 + exp(-x*v));
% y = max(0, x*v);
% y = min(2, max(0, x*v));
y = (x*v).^2;
% y = max(x*v, -0.5 * x*v);
% plot(x(:, 1), x(:, 2), 'ro')
% hold on

y = y + noise_level * randn(size(y));

% 
% plot(v)

% [xtemp, indx] = sort(x*v);
% plot(xtemp, y(indx))
% hold on
% plot(xtemp, y(indx), 'ro')
%% test algorithms

options.k = 3;
% options.lambda = 1e-8;
% options.rescale = 1.1;
% options.nbasis = power(length(x), 0.3);
options.nbasis = 20;
options.reg =0;

options.iter = 200;
% options.gamma = 10;
% options.ninit = 100;
options.vtrue = v;
options.gradient = 2;
options.seed = 100;

[alpha, v_, vdiff, ydiff] = matlab_bspline_single_index(x, y, options);

% [alpha, v, vdiff, ydiff] = bspline_single_index(x, y, options);


% figure 
% plot(vdiff)
% 
% figure
% plot(ydiff)

subspace(v_, v)