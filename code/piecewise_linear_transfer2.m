function y = piecewise_linear_transfer2(x)
%   piecewise linear transfer function
%   y = x, [0, 0.2]
%   y = 0.4-x, (0.2, inf)
%   y = -0.5x, [-0.2, 0)
%   y = 0.7+3x, (-inf, -0.2)

y = zeros(size(x));
idx = 0 <= x & x <=0.2;
y(idx) = x(idx);
idx = x > 0.2;
y(idx) = 0.4 - x(idx);
idx = -0.2 <= x & x < 0;
y(idx) = -0.5*x(idx);
idx = x < -0.2;
y(idx) = 0.7 + 3*x(idx);