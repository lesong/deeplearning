function [s, obj] = convex_regression_nesterov_ls_reg_nosort(x, y, lambda)
% Assumes x is already sorted
% using projected gradient descent to solve
%
%   min_beta 1/(2n) sum_i (y_i - sum_{j<i} beta_j)^2
%   s.t. beta_i <= beta_{i+1}
%
%   x: n x 1
%   y: n x 1

if nargin < 3
    lambda = 0;
end

n = length(y);
eta = 1;
eta_a = 1.05;
eta_b = 2;
fval_tol = 1e-3;
max_iter = 5e3;
interval = 50;

x_diff = [1; diff(x)];
beta = zeros(n, 1);
search_pt = beta;
prev_beta = beta;
prev_alpha = 1;

obj = [];
best_fval = inf;
prev_best_fval = best_fval;
for iter = 1:max_iter
    % evaluate at search point
    tmp = cumsum(search_pt .* x_diff);
    tmp = tmp - y;
    
    % evaluate Delta^t Delta s
    tmp = cumsum(tmp(end:-1:1));
    grad = tmp(end:-1:1) .* x_diff / n;
    grad(2:end) = grad(2:end) + lambda / n * search_pt(2:end);  % l2 regularization
    
    % increase step size
    eta = eta * eta_a;
    continue_line_search = true;
    
    while continue_line_search 
        % gradient step
        beta = search_pt - eta * grad;
        % projection step
        beta(2:end) = lsqisotonic_nosort(x(2:end), beta(2:end));
        
        % check line search condition
        tmp = cumsum(beta .* x_diff); tmp = tmp - y; tmp = cumsum(tmp(end:-1:1));
        tmp_grad = tmp(end:-1:1) .* x_diff / n;
        tmp_grad(2:end) = tmp_grad(2:end) + lambda / n * beta(2:end);
        
        grad_map = search_pt - beta;
        grad_diff = eta * (tmp_grad - grad);
        ls_condition = (grad_map + grad_diff)' * grad_map - sum((grad_map + grad_diff).^2);
        if ls_condition >= 0
            continue_line_search = false;
        else
            % does not satisfy line search condition, reduce step size
            eta = eta / eta_b;
        end
    end
  
    if mod(iter, interval) == 0
        tmp = cumsum(beta .* x_diff);
        tmp = tmp - y;
        fval = (sum(tmp.^2) + lambda*sum(beta(2:end).^2)) / (2*n);
%         fprintf('-- iter: %i / %i, stepsize: %.4f, fval: %.6f\n',...
%             iter, max_iter, eta, fval);
%         fprintf('---- prev_best_fval: %.4f, best_fval: %.4f\n', prev_best_fval, best_fval);
        obj = [obj fval];
        
        if fval < best_fval
            prev_best_fval = best_fval;
            best_fval = fval;
        end
        
        if (prev_best_fval - best_fval) / prev_best_fval < fval_tol
%             fprintf('--prev_best_fval: %.6f, best_fval: %.6f, smaller than tol: %f, converged\n', ...
%                 prev_best_fval, best_fval, fval_tol);
            break;
        end
        
%         % visualize
%         s = cumsum(beta .* x_diff);
%         plot(x, y, 'gx'); hold on
%         plot(x, s, 'b--'); hold off
%         drawnow
    end
    
    % find new search point
    alpha = (1 + sqrt(1+4*prev_alpha^2)) / 2;
    search_pt = beta + (prev_alpha - 1) / alpha * (beta - prev_beta);
    
    prev_alpha = alpha;
    prev_beta = beta;
end

% convert back to function value
s = cumsum(beta .* x_diff);