% clear all; close all; 

generate_data

gpuid = 1;
gpuDevice(gpuid)
cpu_data = data;
cpu_test_data = test_data;
cpu_y = y;
cpu_test_y = test_y;

data = gpuArray(cpu_data);
test_data = gpuArray(cpu_test_data);
y = gpuArray(cpu_y);
test_y = gpuArray(cpu_test_y);

% parameters
iterno = 50;
inner_iterno = 1e4;
interval = 1e3;
eta = 1e-2;
param.a = 0.02;
tol = 1e-8;

% initialization
cpu_ew = zeros(iterno, 1);
cpu_wmat = zeros(d, iterno);
ew = gpuArray(cpu_ew);
wmat = gpuArray(cpu_wmat);

train_err_mat = zeros(iterno, 1); 
test_err_mat = zeros(iterno, 1);
for iter = 1:iterno
    % Add a basis
    if iter == 1
        yy = cpu_y; 
    else
        % make current prediction
        predh = cpu_wmat(:, 1:iter-1)' * cpu_data;
        predh = max(predh, param.a*predh);
        predy = cpu_ew(1:iter-1)' * predh;
        yy = cpu_y - predy;
    end
    
    [curr_w, pos_func] = sample_basis(cpu_data, yy, param);
    
    cpu_wmat(:, iter) = curr_w;
    vis_yy = vis_data'*curr_w;
    vis_yy = max(vis_yy, param.a * vis_yy);
    if ~pos_func
        vis_yy = - vis_yy;
    end
    surf(x1, x2, reshape(vis_yy, [size(x1, 1), size(x1, 1)]));
    drawnow; 
    pause(1);
    
    if pos_func
        cpu_ew(iter) = 1;
    else
        cpu_ew(iter) = -1;
    end
    
    % gradient descent to fine tune
    predy_mat = zeros(iter, n_train);
    prev_yy = zeros(1, n_train);
    
    ew = gpuArray(cpu_ew);
    wmat = gpuArray(cpu_wmat);
    for inner_iter = 1:inner_iterno
        % make current prediction
        predh = wmat(:, 1:iter)' * data;
        predh = max(predh, param.a*predh);
        predy = ew(1:iter)' * predh;
        yy = y - predy;
        
        % calculate gradient and update
        gradew = - predh * yy' / n_train;
        
%         tmp = ew(1:iter) * yy;
%         tmp(predh <= 0) = tmp(predh <= 0) .* param.a;
%         gradwmat = - data * tmp' / n_train;
          
        ew(1:iter) = ew(1:iter) - eta * gradew;
%         wmat(:, 1:iter) = wmat(:, 1:iter) - eta * gradwmat;
        
        % evaluate error
        if mod(inner_iter, interval) == 0
            train_err = mean(yy.^2) / 2;
            
            % test error
            test_predh = wmat(:, 1:iter)' * test_data;
            test_predh = max(test_predh, param.a*test_predh);
            test_predy = ew(1:iter)' * test_predh;
            test_yy = test_y - test_predy;
            
            test_err = mean(test_yy.^2) / 2;
            
            fprintf('-- iter %i, inner_iter %i, train_err, %f, test_err, %f\n',...
                iter, inner_iter, train_err, test_err);
            
            % determine if converged
            if (inner_iter > 2e3)
                old_train_err = mean(prev_yy.^2) / 2;
                change = (old_train_err - train_err) / old_train_err;
                if change < tol
                    break;
                end
            end
        end
        
        prev_yy = yy;
    end
    cpu_ew = gather(ew);
    cpu_wmat = gather(wmat);
    
    train_err_mat(iter) = gather(mean(yy.^2) / 2);
    test_err_mat(iter) = gather(test_err / 2);
end