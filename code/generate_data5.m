% generate data
rng(294016)
k = 150;
d = 100;
wtrue = randn(d, k);
wtrue = bsxfun(@rdivide, wtrue, sqrt(sum(wtrue.^2)));
btrue = 0.2 * randn(k, 1);
% btrue = zeros(k, 1); 
ewtrue = randn(k, 1);
% ewtrue = 5*rand(k, 1);

% [x1, x2] = meshgrid([-3:0.05:1], [-3:0.05:1]);
% vis_data = [x1(:) x2(:)]';
% n_vis = size(vis_data, 2);
% h = bsxfun(@plus, wtrue' * vis_data, btrue);
% h = piecewise_linear_transfer(h);
% vis_y = ewtrue' * h;

% n_train = 2500;
n_train = 5e4;
% data = 1 * randn(d, n_train);
data = rand(d, n_train) ./ sqrt(d) - 1/(2*sqrt(d));
h = bsxfun(@plus, wtrue' * data, btrue);

% h = piecewise_linear_transfer(h);
h = piecewise_linear_transfer2(h);

y = ewtrue' * h + 0.0 * randn(1, n_train);

n_test = 1000;
% test_data = 1 * randn(d, n_test);
test_data = rand(d, n_test) ./ sqrt(d) - 1/(2*sqrt(d)); 
h = bsxfun(@plus, wtrue' * test_data, btrue);

% h = piecewise_linear_transfer(h);
h = piecewise_linear_transfer2(h);

test_y = ewtrue' * h + 0.0 * randn(1, n_test);

% vis_data = [vis_data; ones(1, n_vis)];
data = [data; ones(1, n_train)];
test_data = [test_data; ones(1, n_test)];
d = d + 1;

% figure; 
% surf(x1, x2, reshape(vis_y, [size(x1, 1), size(x1, 1)]));
% drawnow;
% % plot3(data(1, :), data(2, :), y, 'rx')
% % grid on
% 
% pause(1); 