function [ y0, y1 ] = chebyshev_fdf ( range, x, coef )

%*****************************************************************************80
%
%% ECHEBSER1 evaluates a Chebyshev series and first derivative.
%
%  Discussion:
%
%    This function implements a modification and extension of
%    Maess's algorithm.  Table 6.5.1 on page 164 of the reference
%    gives an example for treating the first derivative.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    22 April 2014
%
%  Author:
%
%    Manfred Zimmer
%
%  Reference:
%
%    Charles Clenshaw,
%    Mathematical Tables, Volume 5,
%    Chebyshev series for mathematical functions,
%    London, 1962.
%
%    Gerhard Maess,
%    Vorlesungen ueber Numerische Mathematik II, Analysis,
%    Berlin, Akademie_Verlag, 1984-1988,
%    ISBN: 978-3764318840,
%    LC: QA297.M325.  
%
%  Parameters:
%
%    Input, real X, the evaluation point.
%    a <= X <= b.
%
%    Input, real COEF(NC), the Chebyshev series.
%
%    Input, integer NC, the number of terms in the series.
%    0 < NC.
%
%    Output, real Y0, the value of the Chebyshev series at X.
%
%    Output, real Y1, the value of the 1st derivative of the
%    Chebyshev series at X.
%
a = range(1);
b = range(2);

trunc_rate = 0.0;  % truncate to a line with slope trunc_rate
x_before_a_idx = x < a;
x_after_b_idx = x > b;

x = [x; a; b];
x2 = 2.0 * ( 2.0 * x - a - b ) / ( b - a );
nc = length(coef);
m = length(x);

b0 = coef(nc);
b1 = zeros(m, 1);
b2 = zeros(m, 1);

c0 = coef(nc);
c1 = zeros(m, 1);
c2 = zeros(m, 1);

for i = nc - 1 : -1 : 1

b2 = b1;
b1 = b0;
b0 = coef(i) - b2 + x2 .* b1;

if ( 1 < i )
  c2 = c1;
  c1 = c0;
  c0 = b0 - c2 + x2 .* c1;
end

end

y0 = 0.5 * ( b0 - b2 );
y1 = (c0 - c2) * 2 / (b - a);
fa = y0(end-1);
fb = y0(end);
y0 = y0(1:end-2);
y1 = y1(1:end-2);

x = x(1:end-2);
y0(x_before_a_idx) = fa - trunc_rate * (a - x(x_before_a_idx));
y0(x_after_b_idx) = fb + trunc_rate * (x(x_after_b_idx) - b);
y1(x_before_a_idx | x_after_b_idx) = trunc_rate;
