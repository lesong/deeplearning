% clear all; close all; 

% load_data_hdf5
% y = y / 50;
% test_y = test_y / 50;

rng(8229)
k = 150;
wtrue = randn(2, k);
wtrue = bsxfun(@rdivide, wtrue, sqrt(sum(wtrue.^2)));
btrue = randn(k, 1);
ewtrue = randn(k, 1);

d = 2; 
[x1, x2] = meshgrid([-5:0.2:5], [-5:0.2:5]);
vis_data = [x1(:) x2(:)]';
n_vis = size(vis_data, 2);
h = bsxfun(@plus, wtrue' * vis_data, btrue);
h = max(h, 0);
vis_y = ewtrue' * h;

n_train = 2500;
data = randn(d, n_train);
% [x1_, x2_] = meshgrid([-6:0.2:4], [-4:0.2:6]);
% data = [x1_(:) x2_(:)]';
h = bsxfun(@plus, wtrue' * data, btrue);
h = max(h, 0);
y = ewtrue' * h;

n_test = 1e3;
test_data = randn(d, n_test);
h = bsxfun(@plus, wtrue' * test_data, btrue);
h = max(h, 0);
test_y = ewtrue' * h;

figure; 
surf(x1, x2, reshape(vis_y, [size(x1, 1), size(x1, 1)]));
drawnow; 

pause(1); 

% parameters
iterno = 50;
inner_iterno = 1e5;
interval = 1e3;
eta = 1e-2;
tol = 1e-6;

% initialization
ew = zeros(iterno, 1);
eb = 0;
wmat = zeros(d, iterno);
bmat = zeros(iterno, 1);
chebyshev = cell(iterno, 1);

train_err_mat = zeros(iterno, 1); 
test_err_mat = zeros(iterno, 1);

figure; 

for iter = 1:iterno
    % Add a basis
    if iter == 1
        yy = y; 
    else
        % make current prediction
        predh = bsxfun(@plus, wmat(:, 1:iter-1)' * data, bmat(1:iter-1));
        for i = 1:iter-1
            predh(i, :) = chebyshev_fdf(chebyshev{i}.range, predh(i, :)', chebyshev{i}.coeff)';
        end
        predy = ew(1:iter-1)' * predh + eb;
        yy = y - predy;
    end
    
    fprintf('*** adding basis %d ***\n', iter); 
    [w_pos, b_pos, ch_poly_pos, scale_y_pos, bias_y_pos]...
        = learn_basis_chebyshev(data', yy');
    pos_yy = chebyshev_fdf(ch_poly_pos.range, data'*w_pos+b_pos, ch_poly_pos.coeff);
    pos_yy = scale_y_pos * pos_yy + bias_y_pos;
    vis_yy = chebyshev_fdf(ch_poly_pos.range, vis_data'*w_pos+b_pos, ch_poly_pos.coeff);
    vis_yy = scale_y_pos * vis_yy + bias_y_pos;    
    pos_err = norm(pos_yy' - yy);
    ch_poly_pos.coeff = ch_poly_pos.coeff * sqrt(scale_y_pos);
    
%     keyboard; 
    surf(x1, x2, reshape(vis_yy, [size(x1, 1), size(x1, 1)]));
    drawnow; 
    pause(1); 
    
    ew(iter) = sqrt(scale_y_pos);
    eb = eb + bias_y_pos;
    wmat(:, iter) = w_pos;
    bmat(iter) = b_pos;
    chebyshev{iter} = ch_poly_pos;
    
    % gradient descent to fine tune
    predy_mat = zeros(iter, n_train);
    prev_yy = zeros(1, n_train);
    for inner_iter = 1:inner_iterno
        % make current prediction
        predh = bsxfun(@plus, wmat(:, 1:iter)' * data, bmat(1:iter));
        gradh = zeros(size(predh));
        for i = 1:iter
            [predh(i, :), gradh(i, :)] = chebyshev_fdf(...
                chebyshev{i}.range, predh(i, :)', chebyshev{i}.coeff);
        end
        predy = ew(1:iter)' * predh + eb;
        yy = y - predy;
        
        % calculate gradient and update
        gradew = - predh * yy' / n_train;
        gradeb = - mean(yy);
        
        gradwmat = zeros(d, iter);
        gradbmat = zeros(iter, 1);
        for i = 1:iter
            gradwmat(:, i) = - ew(i) * data * (yy .* gradh(i, :))' / n_train;
            gradbmat(i) = - ew(i) * gradh(i, :) * yy' / n_train;
        end
        
        ew(1:iter) = ew(1:iter) - eta * gradew;
%         eb = eb - eta * gradeb;
%         wmat(:, 1:iter) = wmat(:, 1:iter) - eta * gradwmat;
%         bmat(1:iter) = bmat(1:iter) - eta * gradbmat;
        
        % evaluate error
        if mod(inner_iter, interval) == 0
            train_err = mean(yy.^2) / 2;
            
            % test error
            test_predh = bsxfun(@plus, wmat(:, 1:iter)' * test_data, bmat(1:iter));
            for i = 1:iter
                test_predh(i, :) = chebyshev_fdf(...
                    chebyshev{i}.range, test_predh(i, :)', chebyshev{i}.coeff);
            end
            test_predy = ew(1:iter)' * test_predh + eb;
            test_yy = test_y - test_predy;
            
            test_err = mean(test_yy.^2) / 2;
            
            fprintf('-- iter %i, inner_iter %i, train_err, %f, test_err, %f\n',...
                iter, inner_iter, train_err, test_err);
            
            % determine if converged
            if (inner_iter > 2e3)
                old_train_err = mean(prev_yy.^2) / 2;
                change = (old_train_err - train_err) / old_train_err;
                if change < tol
                    break;
                end
            end
        end
        
        prev_yy = yy;
    end
    
    train_err_mat(iter) = mean(yy.^2) / 2;
    test_err_mat(iter) = test_err / 2;
end