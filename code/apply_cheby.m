function [y, dydx] = apply_cheby(cheby_poly, x)
% Apply Chebyshev function on x
% y = g(x)

[y, dydx] = chebyshev_fdf(cheby_poly.range, x, cheby_poly.coeff);
y = y + cheby_poly.c;