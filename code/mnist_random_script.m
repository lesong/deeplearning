% clear all; close all; 

loadmnistdata

gpuid = 1;
gpuDevice(gpuid)
cpu_data = traindata;
cpu_test_data = testdata;
cpu_y = train_y;
cpu_test_y = test_y;

traindata = gpuArray(cpu_data);
testdata = gpuArray(cpu_test_data);
train_y = gpuArray(cpu_y);
test_y = gpuArray(cpu_test_y);

% parameters
iterno = 50;
inner_iterno = 1e4;
sample_size = 1e4;
interval = 1e3;
eta = 1e-2;
tol = 1e-8;

% initialization
cpu_ew = zeros(iterno, 1);
cpu_wmat = zeros(d, iterno);
ew = gpuArray(cpu_ew);
wmat = gpuArray(cpu_wmat);

train_err_mat = zeros(iterno, 1); 
test_err_mat = zeros(iterno, 1);

for iter = 1:iterno
    % Add a basis
    if iter == 1
        yy = train_y; 
    else
        predh = wmat(:, 1:iter-1)' * traindata;
        predh = max(predh, 0);
        predy = ew(1:iter-1)' * predh;
        yy = train_y - predy;
    end
    
    fprintf('--adding a random basis\n')
    [curr_w, pos_func] = sample_basis2(cpu_data, gather(yy), sample_size);
    fprintf('--finished selecting random basis\n')
    
    wmat(:, iter) = gpuArray(curr_w);
    
    if pos_func
        ew(iter) = 1;
    else
        ew(iter) = -1;
    end
    
    % gradient descent to fine tune
    for inner_iter = 1:inner_iterno
        % make current prediction
        predh = wmat(:, 1:iter)' * traindata;
        predh = max(predh, 0);
        predy = ew(1:iter)' * predh;
        yy = train_y - predy;
        
        % calculate gradient and update
        gradew = - predh * yy' / n_train;
        tmp = ew(1:iter) * yy;
        tmp(predh <= 0) = 0;
        gradwmat = - traindata * tmp' / n_train;
        
        ew(1:iter) = ew(1:iter) - eta * gradew;
        wmat(:, 1:iter) = wmat(:, 1:iter) - eta * gradwmat;
        
        % evaluate error
        if mod(inner_iter, interval) == 0
            train_err = mean(yy.^2) / 2;
            
            % test error
            test_predh = wmat(:, 1:iter)' * testdata;
            test_predh = max(test_predh, 0);
            test_predy = ew(1:iter)' * test_predh;
            test_yy = test_y - test_predy;
            
            test_err = mean(test_yy.^2) / 2;
            
            fprintf('-- iter %i, inner_iter %i, train_err, %f, test_err, %f\n',...
                iter, inner_iter, train_err, test_err);
            
            % determine if converged
            if (inner_iter > 2e3)
                old_train_err = mean(prev_yy.^2) / 2;
                change = (old_train_err - train_err) / old_train_err;
                if change < tol
                    break;
                end
            end
        end
        
        prev_yy = yy;
    end
    
    train_err_mat(iter) = gather(mean(yy.^2) / 2);
    test_err_mat(iter) = gather(test_err / 2);
end