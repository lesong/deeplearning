
function [weights, B] = bspline_nonlinear(k, t, x, y, options)
% B-spline curve control point approximation with known knot vector.
%
% Input arguments:
% param k:    B-spline order (2 for linear, 3 for quadratic, etc.)
% param t:    knot vector, length(t) = n + k
% param x:    m-by-1 vector, data points
% param y:    m-by-1 vector, function values


% evaluate the bspline matrix at x locations, m-by-n
B = bspline_basismatrix(k,t,x);
weights = (B'*B + options.lambda*eye(size(B, 2)))\ B'*y;
