function [alpha, v, vdiff, ydiff] = bspline_single_index(x, y, options)

% INPUT
% param x, y: data points to be interpolated
%             x is n-by-d vector
%             y is n-by-1 vector
% param options: options parameter for the models
%                options.k -- order of B-Spline
%                options.lambda -- regularization for B-spline matrix
%                options.nbasis -- number of basis of B-Spline
%                options.rescale -- rescale the range of projected x for
%                bspline

%                optimization options for out loop
%                options.iter -- number of iterations


% param options.vture: for debug

% OUTPUT
% param alpha: weights for each B-spline basis
% param v: projection direction

[n, d] = size(x);

v = randn(d, 1);
% theta = rand() * pi / 2 
% rotation = [cos(theta), -sin(theta); sin(theta), cos(theta)];
% v = rotation * options.vtrue;

vdiff = zeros(options.iter, 1);
ydiff = zeros(options.iter, 1);

xv = x * options.vtrue;
[xtemp, idx] = sort(xv);

for i = 1: options.iter
     
    % update h
    xproj = x * v;
    gap = max(xproj) - min(xproj);
    intv = options.rescale*min(xproj) : gap / options.nbasis : options.rescale*max(xproj); 
    [alpha, B] = bspline_nonlinear(options.k, intv, xproj, y, options);
    
    yest = B * alpha;
    [xproj_sort, indx] = sort(xproj);
    plot(xproj_sort, yest(indx), 'm')
    hold on
    plot(xproj_sort, yest(indx), 'go')
    plot(xtemp, y(idx));
    plot(xtemp, y(idx), 'r*')
   
    for j = 1: n
       line([xv(j), xproj(j)], [y(j), yest(j)], 'Color', 'c')
    end
    drawnow

    
    % update v
  
    stepsize = 1;
    if options.gradient == 1
        v = (1 - stepsize*options.reg) * v +  stepsize / n * x' * (y - yest);
    else 
%         gtemp = 
        v = (1 - stepsize*options.reg) * v +  stepsize / n * x' * ((y - yest) .* gtemp);
    end
    
    
    vdiff(i) = (v'/norm(v) * options.vtrue./norm(options.vtrue));
    ydiff(i) = norm(yest - y, 2);
    
    if i>1 && abs(ydiff(i) - ydiff(i-1)) < 1e-12
        return
    end
    
    if i ~= options.iter
        clf()
    end
end
