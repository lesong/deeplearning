clear; close all;

% test incremental fitting bspline
addpath('bspline')

rng('default')
generate_data3;

xtrain = data';
ytrain = y';

xtest = test_data';
ytest = test_y';
%%

options.nfunc = 50;

options.k = 3;
options.nbasis = 20;
options.reg = 1e-1;
options.iter = 200;
options.gradient = 2;
options.interval = 4;
options.lambda = 1e-8;
options.stepsize = 1;

options.test = false;
options.plot = false;

[weights, basis_funcs, vproj, residual, test_error] = incremental_fitting_bspline(xtrain, ytrain, xtest, ytest, options);

plot(residual, 'r')
hold on
plot(test_error, 'b')
legend('train error', 'test error')
drawnow

%%
% finetuning
iterno = options.nfunc;
wmat = vproj;
splines = basis_funcs;
ew = weights;

der_splines = cell(iterno, 1);
for i = 1:iterno
    der_splines{i} = fnder(splines{i}, 1);
end

max_iter = 1e5;
interval = 20;
eta = 1e-3;
tol = 1e-8;

prev_yy = zeros(1, n_train);
for iter = 1:max_iter
     % make current prediction
    theta = wmat' * data;
    predh = zeros(size(theta));
    gradh = zeros(size(predh));
    for i = 1:iterno
        theta(i, :) = min(max(theta(i, :), splines{i}.knots(1)), splines{i}.knots(end));
        predh(i, :) = fnval(splines{i}, theta(i, :)');
        gradh(i, :) = fnval(der_splines{i}, theta(i, :)');
    end
    predy = ew' * predh;
    yy = y - predy;

    % calculate gradient and update
    gradew = - predh * yy' / n_train;

    gradwmat = zeros(d, iterno);
    gradcoef = zeros(options.nbasis + options.k - 1, iterno);
    for i = 1:iterno
        gradwmat(:, i) = - ew(i) * data * (yy .* gradh(i, :))' / n_train;
        gradcoef(:, i) = - ew(i) * yy *...
            bspline_basismatrix(options.k, splines{i}.knots, theta(i, :)') / n_train;
    end

    ew = ew - eta * gradew;
    wmat = wmat - eta * gradwmat;
    for i = 1:iterno
        splines{i}.coefs = splines{i}.coefs - eta * gradcoef(:, i)';
        der_splines{i} = fnder(splines{i}, 1);
    end

    % evaluate error
    if mod(iter, interval) == 0
        train_err = mean(yy.^2) / 2;

        % test error
        test_predh = wmat' * test_data;
        for i = 1:iterno
            test_predh(i, :) = min(max(test_predh(i, :), splines{i}.knots(1)), splines{i}.knots(end));
            test_predh(i, :) = fnval(splines{i}, test_predh(i, :)');
        end
        test_predy = ew' * test_predh;
        test_yy = test_y - test_predy;

        test_err = mean(test_yy.^2) / 2;

        fprintf('-- iter %i, train_err, %f, test_err, %f\n',...
            iter, train_err, test_err);
        
        % determine if converged
        if (iter > 2e3)
            old_train_err = mean(prev_yy.^2) / 2;
            change = (old_train_err - train_err) / old_train_err;
            if change < tol
                break;
            end
        end
    end

    prev_yy = yy;
end