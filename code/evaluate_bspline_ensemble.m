function [fval, xnl] = evaluate_bspline_ensemble(weights, basis_funcs, vproj, x)
% evaluate bspline ensemble on x;
% param x: n-by-d matrix;
% param vproj: d-by-options.nfunc matrix;
% param weights: options.nfunc-by-1 vector;
% param basis_func: a cell contains options.nfunc B-spline functions

xproj = x * vproj;

xnl = zeros(size(x, 1), length(weights));
for i = 1: length(weights)
    xnl(:, i) = fnval(basis_funcs{i}, xproj(:, i));
end

fval = xnl * weights;