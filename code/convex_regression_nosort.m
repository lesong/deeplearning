function [s, obj] = convex_regression_nosort(x, y)
% Assumes x is already sorted
% using projected gradient descent to solve
%
%   min_beta 1/(2n) sum_i (y_i - sum_{j<i} beta_j)^2
%   s.t. beta_i <= beta_{i+1}
%
%   x: n x 1
%   y: n x 1

n = length(y);
eta = 1e-1;
max_iter = 1e4;
interval = 50;

x_diff = [1; diff(x)];
beta = [y(1); diff(y)./diff(x)];
% projection step
beta(2:end) = lsqisotonic_nosort(x(2:end), beta(2:end));

obj = [];
for iter = 1:max_iter
    % evaluate Delta beta
    tmp = beta .* x_diff;
    tmp = cumsum(tmp);
    tmp = tmp - y;
    
    if mod(iter, interval) == 0
        fval = sum(tmp.^2) / n / 2;
        fprintf('-- iter: %i / %i, fval: %.6f\n', iter, max_iter, fval);
        obj = [obj fval];
    end
    
    % evaluate Delta^t Delta beta
    tmp = cumsum(tmp(end:-1:1));
    grad = tmp(end:-1:1) .* x_diff / n;
    
    % gradient step
    beta = beta - eta * grad;
    % projection step
    beta(2:end) = lsqisotonic_nosort(x(2:end), beta(2:end));
end

% convert back to function value
s = cumsum(beta .* x_diff);