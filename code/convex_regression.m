function [s] = convex_regression(x, y)
n = length(y);
[xyord, ord] = sortrows([x(:) y(:)]); iord(ord) = 1:n;

[s] = convex_regression_nesterov_ls_reg_nosort(xyord(:, 1), xyord(:, 2));

s = s(iord);