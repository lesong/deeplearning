function [s, obj] = convex_regression_nesterov_ls_nosort(x, y)
% Assumes x is already sorted
% using projected gradient descent to solve
%
%   min_beta 1/(2n) sum_i (y_i - sum_{j<i} beta_j)^2
%   s.t. beta_i <= beta_{i+1}
%
%   x: n x 1
%   y: n x 1

n = length(y);
eta = 1;
eta_a = 1.2;
eta_b = 2;
max_iter = 1e4;
interval = 50;

x_diff = [1; diff(x)];
beta = [y(1); diff(y)./diff(x)];
% projection step
beta(2:end) = lsqisotonic_nosort(x(2:end), beta(2:end));
search_pt = beta;
prev_beta = beta;
prev_alpha = 1;

obj = [];
for iter = 1:max_iter
    % evaluate at search point
    tmp = cumsum(search_pt .* x_diff);
    tmp = tmp - y;
    
    % evaluate Delta^t Delta s
    tmp = cumsum(tmp(end:-1:1));
    grad = tmp(end:-1:1) .* x_diff / n;
    
    % increase step size
    eta = eta * eta_a;
    continue_line_search = true;
    
    while continue_line_search 
        % gradient step
        beta = search_pt - eta * grad;
        % projection step
        beta(2:end) = lsqisotonic_nosort(x(2:end), beta(2:end));
        
        % check line search condition
        tmp = cumsum(beta .* x_diff); tmp = tmp - y; tmp = cumsum(tmp(end:-1:1));
        tmp_grad = tmp(end:-1:1) .* x_diff / n;
        
        grad_map = search_pt - beta;
        grad_diff = eta * (tmp_grad - grad);
        ls_condition = (grad_map + grad_diff)' * grad_map - sum((grad_map + grad_diff).^2);
        if ls_condition >= 0
            continue_line_search = false;
        else
            % does not satisfy line search condition, reduce step size
            eta = eta / eta_b;
        end
    end
  
    if mod(iter, interval) == 0
        tmp = cumsum(beta .* x_diff);
        tmp = tmp - y;
        fval = sum(tmp.^2) / n / 2;
        fprintf('-- iter: %i / %i, stepsize: %.4f, fval: %.6f\n', iter, max_iter, eta, fval);
        obj = [obj fval];
    end
    
    % find new search point
    alpha = (1 + sqrt(1+4*prev_alpha^2)) / 2;
    search_pt = beta + (prev_alpha - 1) / alpha * (beta - prev_beta);
    
    prev_alpha = alpha;
    prev_beta = beta;
end

% convert back to function value
s = cumsum(beta .* x_diff);