function [w, theta, g] = bregman_convex_learn(x, y, init_w)
% Learns w through marginalized gradient descent
% by minimizing
%
% loss(w, phi) = phi(y) - phi(g(x*w)) - <x*w, y - g(x*w)>
%
% where g is the piecewise linear function that matches phi
% and is obtained by isotonic regression with constraints l and u.
%
% The algorithm does gradient descent on w
% the gradient is
% grad = x' [g(x*w) - y]
% the current g is calculated by isotonic regression
% with current w.
%
%   x: n x d
%   y: n x 1
%   w: d x 1
%   theta: n x 1
%   g: n x 1
%   theta and g together define the function

n = length(y);
d = size(x, 2);
max_iter = 100;
interval = 1;
eta = 1; 
tol = 1e-8; 

if nargin == 3
    w = init_w;
else
    w = randn(d, 1);
    w = w ./ norm(w); 
end

min_err = Inf; 
min_w = w; 
prev_a = 1;
prev_search_pt = w;
old_w = w; 

figure
for iter = 1:max_iter
    % perform isotonic regression
    theta = x * w;
%     g0 = constrained_convex_qp(theta, y);
    g = convex_regression(theta, y);
    hold off
    plot(theta, y, 'gx'); hold on
%     plot(theta, g0, 'bv')
    plot(theta, g, 'ro')    
    drawnow
    
    err = mean((g-y).^2);
    
    if err < min_err
      min_w = w; 
      min_err = err; 
    end    

    % sort the functions according to theta
    [theta_g_ord, ord] = sortrows([theta(:) g(:)]); iord(ord) = 1:n;
    grad_g = diff(theta_g_ord(:, 2)) ./ diff(theta_g_ord(:, 1));
    grad_g = [grad_g; grad_g(end)];
    grad_g = grad_g(iord);
    
%     grad = x' * (g - y) / n; 
    grad = x' * ((g - y) .* grad_g) / n; 
    search_pt = w - eta * grad;
    a = (1 + sqrt(4*prev_a^2+1)) / 2;    
    w = search_pt;
%     keyboard
    
    if mod(iter, interval) == 0
        fprintf('--%i/%i, err = %.6f, coeff=%.6f, norm change=%.6f\n',...
            iter, max_iter, min_err, (prev_a-1) / a, norm(old_w - w)/norm(old_w));
    end
    
    if (norm(old_w - w) / norm(old_w)) < tol
      break; 
    end
end
% this is a slight misnormer, but according to
% nesterov' gradient descent, the final solution
% should be search_pt defined here.
w = min_w;

if nargout > 1
    theta0 = x * w;
    g0 = constrained_convex_qp(theta0, y);
    out = sortrows([theta0(:) g0(:)]);
    theta = out(:, 1);
    g = out(:, 2);
end