function w = bregman_isotonic_learn_old(x, y, l, u)
% Learns w through marginalized gradient descent
% by minimizing
%
% loss(w, phi) = phi(y) - phi(g(x*w)) - <x*w, y - g(x*w)>
%
% where g is the piecewise linear function that matches phi
% and is obtained by isotonic regression with constraints l and u.
%
% The algorithm does gradient descent on w
% the gradient is
% grad = x' [g(x*w) - y]
% the current g is calculated by isotonic regression
% with current w.
%
%   x: n x d
%   y: n x 1
%   w: d x 1

n = length(y);
d = size(x, 2);
max_iter = 5e3;
interval = 40;
tol = 1e-3;
eta = 1/u;    % some heuristic

w = randn(d, 1);
for iter = 1:max_iter
    % perform isotonic regression
    theta = x * w;
    g = constrained_isotonic(theta, y, l, u);
    grad = x' * (g - y) / n;
    
    w = w - eta * grad;
    
%     wnorm = norm(w);
%     if wnorm > 1
%         w = w / wnorm;
%     end
    
    gradnorm = norm(grad);
    change = eta * gradnorm / norm(w);
    if change < tol
        fprintf('--finished %i/%i, gradnorm = %.6f\n',...
            iter, max_iter, gradnorm);
        break;
    end
    
    if mod(iter, interval) == 0
        fprintf('--%i/%i, gradnorm = %.6f\n',...
            iter, max_iter, gradnorm);
    end
end