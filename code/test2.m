% test bregman isotonic learn
close all; clear; 

d = 200;
n = 1e5;

% rng(4950)
wtrue = randn(d, 1);
x = 2*rand(n, d) - 1;
% x = randn(n, d); 
% x = [x ones(n, 1)];
theta = x * wtrue;

a = 0.9999;
y = abs(theta) + 0.0*randn(size(theta));
% y = max(theta, 0) - min(a*theta, 0) + 1*randn(size(theta));
% y = max(theta, 0) + 0.1*randn(size(theta));
% y = sin(8*theta) + 0.1 * randn(size(theta)); 


% load residuedata
% x = x';
% y = y';


% figure; 
% plot3(x(:, 1), x(:, 2), y, 'bx'); grid on; hold on;
% drawnow; 

% l = 0.05;
l = 1e-2;
u = 10;
% w2 = bregman_isotonic_learn(x, y, l, u);
w2 = perceptron_isotonic_learn2(x, y, l, u);
% w = bregman_isotonic_learn_old(x, y, l, u);
w3 = perceptron_isotonic_learn3(x, y, l, u);

% cos(subspace(wtrue, w2))
% cos(subspace(wtrue, w3))
subspace(wtrue, w2)
subspace(wtrue, w3)

theta2 = x * w2;
% g2 = constrained_isotonic(theta2, y, l, u);

g2 = lsqisotonic(theta2, y); 

theta3 = x * w3;
g3 = lsqisotonic(theta3, y);

% g3 = constrained_isotonic_qp(theta2, y, u); 

% plot3(x(:, 1), x(:, 2), g3, 'rx'); 
% drawnow; 

figure;
plot(theta, y, 'g.')
hold on; 
plot(theta2, g2, 'rx'); 
plot(theta3, g3, 'b.'); 

% g3 = constrained_isotonic_qp(theta2, y, u);


% param.a = 0.02;
% % % [w, pos_func] = relu_bregman_fit2d(x', y', param, wtrue);
% % [w3, pos_func] = relu_bregman_fit2d(x', y', param);
% % theta3 = x * w3;
% % g3 = max(theta3, theta3*param.a);
% % 
% % plot3(x(:, 1), x(:, 2), g3, 'rx');
% 
% 
% [w4, pos] = sample_basis(x', y', param);
% theta4 = x * w4;
% g4 = max(theta4, theta4*param.a);
% plot3(x(:, 1), x(:, 2), g4, 'gx');
% 
% 
% [f3, fgrad3] = convex_bregman_divergence(w2, y, x, param);
% [f4, fgrad4] = convex_bregman_divergence(w4, y, x, param);
% 
% fgrad3
% fgrad4
% 
% f3
% f4