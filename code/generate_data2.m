% generate data
% rng(294016)
rng(294018)
k = 150;
% d = 50;
d = 2;
wtrue = randn(d, k);
wtrue = 2 * bsxfun(@rdivide, wtrue, sqrt(sum(wtrue.^2)));
btrue = randn(k, 1);
ewtrue = randn(k, 1);

[x1, x2] = meshgrid([-1:0.05:1], [-1:0.05:1]);
vis_data = [x1(:) x2(:)]';
n_vis = size(vis_data, 2);
h = bsxfun(@plus, wtrue' * vis_data, btrue);
h = max(h, 0);
vis_y = ewtrue' * h;

n_train = 2500;
n_train = 800;
% data = randn(d, n_train);
data = rand(d, n_train)*2 - 1;
h = bsxfun(@plus, wtrue' * data, btrue);
h = max(h, 0);
y = ewtrue' * h;

n_test = 1e3;
% test_data = randn(d, n_test);
test_data = 2*rand(d, n_test) - 1;
h = bsxfun(@plus, wtrue' * test_data, btrue);
h = max(h, 0);
test_y = ewtrue' * h;

vis_data = [vis_data; ones(1, n_vis)];
data = [data; ones(1, n_train)];
test_data = [test_data; ones(1, n_test)];
d = d + 1;

figure; 
surf(x1, x2, reshape(vis_y, [size(x1, 1), size(x1, 1)]));
drawnow; 

pause(1); 