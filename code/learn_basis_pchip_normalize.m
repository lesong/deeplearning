function [w, pchip_pp] = learn_basis_pchip_normalize(x, y, l, u)
% Learn a function basis using isotonic regression and 
%   represent it as a piecewise pchip
% 
%   y = f(w'x)
%
% input:
%   x: n x d
%   y: n x 1
%
% output:
%   w: d x 1
%   pchip_pp: a struct
%       it represents a pchip
%       .deriv: it represents the derivative of a pchip

if nargin <= 2
    l = 0.01;
    u = 10;
end

p = 20;
k = 4;

miny = min(y);
maxy = max(y);
scale_y = maxy - miny;
bias_y = miny;
y = (y - bias_y) / scale_y;     % normalize to [0, 1]

[w, theta, g] = bregman_isotonic_learn(x, y, l, u);

g = scale_y * g + bias_y;

% unif_knots = linspace(min(theta), max(theta), p);
% sp = spap2(augknt(unif_knots, k), k, theta, g);
sp = spap2(p, k, theta, g);
spgood = spap2(newknt(sp), k, theta, g);
spxx = unique(spgood.knots);
spyy = interp1(theta, g, spxx);
pchip_pp = pchip(spxx, spyy);
pchip_pp.deriv = fnder(pchip_pp, 1);
pchip_pp.l_y = ppval(pchip_pp, pchip_pp.breaks(1));
pchip_pp.l_deriv = ppval(pchip_pp.deriv, pchip_pp.breaks(1));
pchip_pp.r_deriv = ppval(pchip_pp.deriv, pchip_pp.breaks(end));
pchip_pp.r_y = ppval(pchip_pp, pchip_pp.breaks(end));