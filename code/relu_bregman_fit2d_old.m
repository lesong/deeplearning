function [w, pos] = relu_bregman_fit2d_old(x, y, param, winit)
if nargin >= 4
    w0 = winit;
else
    w0 = randn(size(x, 1), 1);
end

pos_myfun = @(w) convex_bregman_divergence(w, y', x', param);
opts = optimset('GradObj','on');
% opts = optimset('GradObj','on', 'DerivativeCheck', 'on');
[w_pos] = ...
    fminunc(pos_myfun, w0, opts);

neg_myfun = @(w) convex_bregman_divergence(w, -y', x', param);
opts = optimset('GradObj','on');
% opts = optimset('GradObj','on', 'DerivativeCheck', 'on');
[w_neg] = ...
    fminunc(neg_myfun, w0, opts);

pos_pred = w_pos' * x;
pos_pred = max(pos_pred, param.a * pos_pred);
pos_err = mean((y - pos_pred).^2);

neg_pred = w_neg' * x;
neg_pred = max(neg_pred, param.a * neg_pred);
neg_err = mean((-y - neg_pred).^2);

if pos_err < neg_err
    pos = 1;
    w = w_pos;
    fprintf('--learned basis error: %.6f\n', pos_err);
else
    pos = 0;
    w = w_neg;
    fprintf('--learned basis error: %.6f\n', neg_err);
end