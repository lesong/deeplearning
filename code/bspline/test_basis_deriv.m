
% spline order
k = 4;
% knot sequence
t = [0 0 0 0 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1 1 1];
% control points
P = [ 0.1993 0.4965 0.6671 0.7085 0.6809 0.2938 0.1071 0.3929 0.5933 0.8099 0.8998 0.8906];


C = bspline_deboor(k,t,P, t);
[dt,dP] = bspline_deriv(k,t,P);
dC = bspline_deboor(k-1,dt,dP, dt);


figure
plot(t, C, 'r')
hold on
plot(dt, dC, 'm')