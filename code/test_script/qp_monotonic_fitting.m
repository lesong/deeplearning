function [alpha, z, optval] = qp_monotonic_fitting(x, y, options)

% INPUT
% param x, y: data points to be interpolated
%             x is n-by-1 vector
%             y is n-by-1 vector
% param options: optimization options for QP solver
%                options.TolCon -- tolerance for QP solver
%                options.MaxIter -- maximum iteration for QP solver
%                options.Algorithm -- algorithm for QP solver
%                options.Display
%                options.fb -- bound for convex function value
%                options.gb -- bound for gradient value

% OUTPUT
% param alpha: alpha = [yest, g] 2n-by-1 vector, first n elements are the
% value of the function, following n elements are the gradients
% param z: sorted x, n-by-1 vector

% implemention of 1D convex regression with reduced constraints.

[z, indx] = sort(x);
ytrain = y(indx);
n = length(z);

% construct the constraints matrix
row_idx = 1:n-1;
row_idx = repmat(row_idx, 1, 2);
col_idx = [1:n-1, 2:n];

A = sparse(row_idx, col_idx, [ones(1, n-1), -ones(1, n-1)], n-1, n);

% Solving the QP
% alpha = quadprog(H,f,A,b) attempts to solve the quadratic programming 
%   problem:
% 
%            min 0.5*alpha'*H*alpha + f'*alpha   subject to:  A*alpha <= b 
%           alpha
% alpha = [yest, gpos, gneg]
% construct H, f, A, b 

H = speye(n);
b = zeros(n-1, 1);
alpha = zeros(size(ytrain));

 % convex or concave
[alpha, optval, flg] = quadprog(H, -ytrain, A, b, [], [], ...
    [-options.fb*ones(n,1)], [options.fb*ones(n,1)], alpha, options);


% compute the gradient 
diff = z(2:n) - z(1:n-1);
g = (alpha(2:n) - alpha(1:n-1)) ./ (diff);
g = [g; g(n-1)];

% test_temp = alpha(1:n-1) + g(1:n-1) .* diff;
% norm(test_temp - alpha(2:n))

alpha = [alpha; g];
% 
% plot(z, alpha(1:n), 'm')
% drawnow

% [ffff, ~] = monotonic_evaluation(alpha, z, 1, z);
% norm(ffff - alpha(1:length(z)))

