function [alpha, z, optval, isconvex] = qp_1d_reduced_constraints(x, y, options)

% INPUT
% param x, y: data points to be interpolated
%             x is n-by-1 vector
%             y is n-by-1 vector
% param options: optimization options for QP solver
%                options.TolCon -- tolerance for QP solver
%                options.MaxIter -- maximum iteration for QP solver
%                options.Algorithm -- algorithm for QP solver
%                options.Display
%                options.fb -- bound for convex function value
%                options.gb -- bound for gradient value

% OUTPUT
% param alpha: alpha = [yest, g] 2n-by-1 vector, first n elements are the
% value of the function, following n elements are the gradients
% param z: sorted x, n-by-1 vector
% param isconvex:  indicate the convexity or cancavity

% implemention of 1D convex regression with reduced constraints.

[z, indx] = sort(x);
ytrain = y(indx);
n = length(z);

% construct the constraints matrix
diff = z(2:n) - z(1:n-1);
gap_diff = z(3:n) - z(1:n-2);


row_idx = 1:n-2;
row_idx = repmat(row_idx, 1, 3);
col_idx = [1:n-2, 2:n-1, 3:n];

A = sparse(row_idx, col_idx, [diff(2:n-1), -gap_diff, diff(1:n-2)]);

% Solving the QP
% alpha = quadprog(H,f,A,b) attempts to solve the quadratic programming 
%   problem:
% 
%            min 0.5*alpha'*H*alpha + f'*alpha   subject to:  A*alpha <= b 
%           alpha
% alpha = [yest, gpos, gneg]
% construct H, f, A, b 

H = speye(n);
b = zeros(n-2, 1);
alpha = zeros(size(ytrain));
isconvex = true;

 % convex or concave
[alpha, optval, flg] = quadprog(H, -ytrain, -A, b, [], [], ...
    [-options.fb*ones(n,1)], [options.fb*ones(n,1)], alpha, options);


% [alpha_ccv, optval_ccv, flg] = quadprog(H, -ytrain, A, b, [], [], ...
%     [-options.fb*ones(n,1)], [options.fb*ones(n,1)], alpha, options);
% 
% if optval > optval_ccv
%     alpha = alpha_ccv;
%     isconvex = false;
%     optval = optval_ccv;
% end


% compute the gradient 
g = (alpha(2:n) - alpha(1:n-1)) ./ (diff);
g = [g; g(n-1)];


alpha = [alpha; g];

% plot(z, alpha(1:n), 'm')
% plot(z, y(indx), 'go')
% drawnow

