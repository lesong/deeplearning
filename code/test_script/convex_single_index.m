
function [alpha, zpos, v, isconvex, vdiff, ydiff] = convex_single_index(x, y, options)

% INPUT
% param x, y: data points to be interpolated
%             x is n-by-d vector
%             y is n-by-1 vector
% param options: parameters of algorithms
%                options.gradient -- 1 : use gradient
%                                    2 : perceptron update
%                optimization options for QP solver
%                options.TolCon -- tolerance for QP solver
%                options.MaxIter -- maximum iteration for QP solver
%                options.Algorithm -- algorithm for QP solver
%                options.Display
%                options.fb -- bound for convex function value
%                options.gb -- bound for gradient value

%                options for out loop
%                options.iter -- number of iterations

% param options.vture: for debug

% OUTPUT
% param alpha: alpha = [yest, g] 2n-by-1 vector, first n elements are the
% value of the function, following n elements are the gradients
% param zpos: sorted x*v, n-by-1 vector
% param v: projection direction
% param isconvex:  indicate the convexity or cancavity

[n, d] = size(x);

% randn('seed', options.seed)
% v = randn(d, 1);
% v = v ./ norm(v);
% acos(v'*options.vtrue)
rand('seed', options.seed);
theta = rand() * pi / 2 
rotation = [cos(theta), -sin(theta); sin(theta), cos(theta)];
v = rotation * options.vtrue;
% v = options.vtrue;

vdiff = zeros(options.iter, 1);
ydiff = zeros(options.iter, 1);
obj = zeros(options.iter, 1);

xv = x * options.vtrue;
[xtemp, idx] = sort(xv);

for i = 1: options.iter
    
    % update h
    xproj = x * v;
    [alpha, zpos, optval, isconvex] = qp_1d_reduced_constraints(xproj, y, options);
    plot(zpos, alpha(1:n), 'm')
    hold on
    [~, indx] = sort(xproj);
    iord(indx) = 1:n;
    yy = alpha(iord);
    plot(zpos, alpha(1:n), 'go')
    plot(xtemp, y(idx));
    plot(xtemp, y(idx), 'r*')

    for j = 1: n
       line([xv(j), xproj(j)], [y(j), yy(j)], 'Color', 'c')
    end
    drawnow

    
    % update v
    % ftt = pwl_evaluation(alpha, zpos, v, x);
    hout = alpha(iord);
    % norm(ftt - alpha(iord)) / length(ftt)
    % stepsize = options.gamma / (options.ninit + sqrt(i));
    stepsize = 1;
    if options.gradient == 1
        v = v +  stepsize / n * x' * (y - hout);
    else
        gtemp = alpha(n+1:end);
        v = v +  stepsize / n * x' * ((y - hout) .* gtemp(iord));
    end
    
    vdiff(i) = (v'/norm(v) * options.vtrue./norm(options.vtrue));
    ydiff(i) = norm(hout - y, 2);
    obj(i) = optval;
    
    if i>1 && abs(obj(i) - obj(i-1)) < 1e-12
        return
    end
    
    if i ~= options.iter
        clf()
    end
end

% figure 
% plot(obj)

