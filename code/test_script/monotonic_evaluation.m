
function [f, g] = monotonic_evaluation(alpha, zpos, v, x)

% INPUT
% param alpha: 2n-by-1 vector. First half saves the points values, second
% half saves the corresponding gradients
% param zpos: n-by-1 vector. Locations of the monotonic function
% param v: d-by-1 vector, projection direction.
% param x: m-by-d vector. The m points need to be evaluated.

% OUTPUT
% param f: m-by-1 vector, the corresponding PWL function values
% param g: m-by-1 vector, the corresponding PWL gradients

xproj = x * v;
[z, indx] = sort(xproj);
iord(indx) = 1: length(z);

n = length(zpos);

f = zeros(length(xproj), 1);
g = zeros(length(xproj), 1);

pre_pos = 1;
for i = 1: length(xproj)
    temp_pos = find(zpos(pre_pos:end)>z(i));
    if ~isempty(temp_pos) 
        pre_pos = pre_pos + temp_pos(1)- 1;
        
        if pre_pos > 1

            g(i) = alpha(pre_pos + n -1);
            f(i) = alpha(pre_pos-1) + g(i) * (z(i) - zpos(pre_pos-1));
        else
            g(i) = alpha(n+1);
            f(i) = alpha(1) + g(i) * (z(i) - zpos(1));
        end
            
    else
        g(i) = alpha(end);
        f(i) = alpha(n) + g(i) * (z(i) - zpos(end));
    end
end


f = f(iord);
g = g(iord);