
%% test monotonic_single_index

clear; close all
clc

% randn('seed', 20)
x = randn(400,2);
% x = [-4:0.02:4];
% x = repmat(x',1 ,2);

v = randn(2, 1);

y = 1 ./ (1 + exp(-x*v));
% y = max(0, x*v);
% plot(x(:, 1), x(:, 2), 'ro')
% hold on
% 
% plot(v)

[xtemp, indx] = sort(x*v);
plot(xtemp, y(indx))
hold on
plot(xtemp, y(indx), 'ro')
%% test algorithms
options.MaxIter = 1000; 
% options.Display = 'iter'; 
% options.Display = 'final'; 
options.Display = 'off';
options.Algorithm = 'interior-point-convex'; 
options.TolCon = 1e-6; 

options.gb = 100;
options.fb = 100;

options.iter = 200;
options.gamma = 10;
options.ninit = 10;
options.vtrue = v;
options.method = 2;

options.gradient = 1;
options.seed = 100;

[alpha, zpos, vlearn, vdiff, ydiff] = monotonic_single_index(x, y, options);


% %% plot figure
figure
[xtest, indx] = sort(x*vlearn);
[ytest, g] = monotonic_evaluation(alpha, zpos, 1, xtest);
plot(xtest, ytest, 'LineWidth', 10)
hold on
plot(xtest, y(indx), 'g*')

figure 
plot(vdiff)

figure
plot(ydiff)