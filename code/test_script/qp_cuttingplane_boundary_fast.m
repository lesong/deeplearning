function [alpha, optval, Apool, isconvex] = qp_cuttingplane_boundary_fast(x, y, options)

% INPUT
% param x, y: data points to be interpolated
%             x is d-by-n matrix
%             y is n-by-1 vector
% param options: optimization options for QP solver
%                options.TolCon -- tolerance for QP solver
%                options.MaxIter -- maximum iteration for QP solver
%                options.Algorithm -- algorithm for QP solver
%                options.Display
%                options.fb -- bound for convex function value
%                options.gb -- bound for gradient value

% OUTPUT
% param alpha: alpha = [yest, g] n-by-(1 + d + d) matrix
% param a:  the weights for result linear functions, d-by-k matrix
% param threshold:  the bias for result linear functions, 1-by-k vector 

% implementation of the modified version of cutting plan with heuristic constrain in 
% G. Balazs, A. Gyorgy and C. Szepesvari, "Near-optimal max-affine
% estimators for convex regression", AISTAT 2015
% We do not consider the constraints on function values over whole domain

%%%%%%%%%%%%%%% TODO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  constraints for function values over whole domain:
%  1, decompose g to gpos and gneg
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[d, n] = size(x);

% compute the constrain beforehand
tmpdiff = kron(x, ones(1,n)) - repmat(x, 1, n); 
didx = sub2ind([n,n], 1:n, 1:n); 
idx = setdiff(1:n*n, didx);

% Gpool = sparse(kron(0:d:(n-1)*d, ones(1,(n-1)*d)) + repmat((1:d), 1, n*(n-1)),  ...
%   kron(1:n*(n-1), ones(1,d)), ...
%   reshape(-tmpdiff(:,idx), 1, d*length(idx)));
% Gpool = Gpool'; 

Gpool = sparse(kron(1:n*(n-1), ones(1,d)), ...
    kron(0:d:(n-1)*d, ones(1,(n-1)*d)) + repmat((1:d), 1, n*(n-1)),  ...
    reshape(-tmpdiff(:,idx), 1, d*length(idx)));


A0 = [ones(n-1,1), -speye(n-1,n-1)]; 
Apool = sparse((n-1)*n, n); 
for i = 1:n
  Apool(((i-1)*(n-1)+1):(i*(n-1)), :) = circshift(A0, [i-1, i-1]); 
end
Apool = [Apool, Gpool]; 

% init_idx = randsample(n*(n-1), fix(n/10));
% A = Apool(init_idx, :);

% construct convex hull for each x
covhull_idx = zeros(n, 2*d);
for i = 1: n
    temp = tmpdiff(:, (i-1)*n + (1: n));
    temppos = max(temp, 0);
    tempneg = min(temp, 0);
    
    temppos(temppos == 0) = nan;
    tempneg(tempneg == 0) = nan;
     
    [lval, kl] = min(temppos, [], 2);
    [uval, ku] = max(tempneg, [], 2);
    
    lnanidx = find(isnan(lval));
    unanidx = find(isnan(uval));
    
    kl(lnanidx) = i;
    ku(unanidx) = i;
    % kl(unanidx) = i;
    % ku(lnanidx) = i;
    
    covhull_idx(i, :) = [kl', ku'];
end

% construct the agglomerative sample
aggx = zeros(d, n);
for i = 1: n
    aggx(:, i) = sum(x(:, covhull_idx(i, :)), 2); 
end
aggx = aggx - 2 * d * x;

aggx(abs(aggx)<options.TolCon)=0;

% constuct matrix for heuristic linear constraints
% G = zeros(n, d*n);
% for i = 1: n
%     G(i, (i-1)*n + (1:d)) = aggx(:, i)'; 
% end

G = sparse(kron(1:n, ones(1, d)), kron(0:d:(n-1)*d, ones(1, d)) + repmat(1:d, 1, n), reshape(aggx, 1, d*n));

A = zeros(n, n);
for i = 1:n
    for j = 1:length(covhull_idx(i, :))
        A(i, covhull_idx(i, j)) = A(i, covhull_idx(i, j)) -1;
    end
end
A(didx) = 2 * d + A(didx);

A = [A, G];

original_A = size(A, 1);

Afull = [A; Apool];
active_set = zeros(size(Afull, 1), 1);
active_set(1: original_A) = 1;

randcon_idx = randsample(size(Apool, 1), 2 * original_A);
active_set(randcon_idx + original_A) = 1;

inactive_set = zeros(size(Afull, 1), 1);
% inrandcon_idx = setdiff(1:size(Apool, 1), randcon_idx);
inactive_set(original_A + (1:size(Apool, 1))) = 1;

% delete zero row
% A(~any(A, 2), :) = [];

% %  alpha = quadprog(H,f,A,b) attempts to solve the quadratic programming 
% %   problem:
% % 
% %            min 0.5*alpha'*H*alpha + f'*alpha   subject to:  A*alpha <= b 
% %           alpha
% %  alpha = [yest, gpos, gneg]
% % construct H, f, A, b 

H = sparse(1:n, 1:n, ones(1,n), n+d*n, n+d*n); 
f = [y(:); zeros(d*n,1)];


% prepare the parameters for QP solver

% options.MaxIter = 1000; 
% % options.Display = 'iter'; 
% options.Display = 'final'; 
% options.Algorithm = 'interior-point-convex'; 
% options.TolCon = 1e-6; 

if ~isfield(options, 'fb')
    options.fb = Inf;
end

if ~isfield(options, 'gb')
    options.gb = max(abs(tmpdiff(:)));
end

alpha = zeros(size(f));
alpha1 = zeros(size(f));

isconvex = true;


% fprintf('   iteration       # of constraints           objval\n');
% fprintf('------------------------------------------------------------------\n');


for optiter = 1 : n    
    
    b = zeros(length(find(active_set>0)), 1);
    % solve QP
    
    if optiter == 1
        
        % convex or concave
        [alpha, optval, flg] = quadprog(H, -f, Afull(active_set>0, :), b, [], [], ...
            [-options.fb*ones(n,1);-options.gb*ones(n*d,1)], [options.fb*ones(n,1); options.gb*ones(n*d,1)], ...
            alpha, options);
        
        [alpha1, optval1, flg1] = quadprog(H, -f, -Afull(active_set>0, :), b, [], [], ...
            [-options.fb*ones(n,1);-options.gb*ones(n*d,1)], [options.fb*ones(n,1); options.gb*ones(n*d,1)], ...
            alpha1, options);
        
        
        if optval > optval1
            alpha = alpha1;
            Afull = -Afull;
            Apool = -Apool;
            isconvex = false;
        end
        
        % fprintf('       %d                %d                  %f\n', optiter, length(b), optval);
    else
        
        [alpha, optval, flg] = quadprog(H, -f, Afull(active_set>0, :), b, [], [], ...
            [-options.fb*ones(n,1);-options.gb*ones(n*d,1)], [options.fb*ones(n,1); options.gb*ones(n*d,1)], ...
            alpha, options);
        
        % fprintf('       %d                %d                  %f\n', optiter, length(b), optval);
    end
    
    Aw = Afull(inactive_set>0, :) * alpha;
    
    if sum((Aw>options.TolCon)) == 0
        break;
    end
    
    for i = 1: n
        % add one constraint into the optimization
        if sum(Aw((i-1)*(n-1) + (1:n-1)) > options.TolCon) > 0
            [~, active_con] = max(Aw((i-1)*(n-1) + (1:n-1)));
            % A = [A; Apool((i-1) * (n-1) + active_con, :)];           
            active_set(original_A  + (i-1)*(n-1) + active_con) = 1;
            
            % delete the violate neighborhood
            %  temp = Apool((i-1) * (n-1) + active_con, 1:n);
            %  if isconvex
            %      active_j = find(temp == -1);
            %  else
            %      active_j = find(temp == 1);
            %  end

            if active_con>=i
                active_j = active_con + 1;
            else
                active_j = active_con;
            end
            
            neighbor_idx = find(covhull_idx(i, :) == active_j);
            if ~isempty(neighbor_idx)
                Afull(i, active_j) = 0;
                if isconvex
                    Afull(i, i) = Afull(i, i) - 1;
                    Afull(i, n + (i-1)*d+(1:d)) = Afull(i, n + (i-1)*d+(1:d)) - x(:, active_j)' + x(:, i)';
                else
                    Afull(i, i) = Afull(i, i) + 1;
                    Afull(i, n + (i-1)*d+(1:d)) = Afull(i, n + (i-1)*d+(1:d)) + x(:, active_j)' - x(:, i)';
                end
                covhull_idx(i, neighbor_idx) = -1; % delete from neighborhood
            end
       end
    end
    
%     wy = alpha(1:length(y));
%     g = alpha(length(y)+1: end);
%         
%     predy = zeros(length(g), n);
%     for i = 1:length(g)
%         predy(i,:) = wy(i) + ...
%             g((i-1)*d + (1:d))' * bsxfun(@minus, x, x(:,i));
%     end
%     
%     if (isconvex)
%         mpredy0 = max(predy, [], 1);
%     else
%         mpredy0 = min(predy, [], 1);
%     end
%     plot(x, mpredy0, 'r*')
%     drawnow
%     
end


Aw = A * alpha;

sum(abs(Aw) <= options.TolCon)

wy = alpha(1:length(y));
plot(x, wy, 'r*')

% 
% wy = alpha(1:length(y));
% g = alpha(length(y)+1: end);
% 
% predy = zeros(length(g), n);
% for i = 1:length(g)
%     predy(i,:) = wy(i) + ...
%         g((i-1)*d + (1:d))' * bsxfun(@minus, x, x(:,i));
% end
% 
% if (isconvex)
%     mpredy0 = max(predy, [], 1);
% else
%     mpredy0 = min(predy, [], 1);
% end
% plot(x, mpredy0, 'r*')
% drawnow