
%% test convex_single_index

clear; close all
clc

randn('seed', 10)
x = randn(400,2);
% x = [-4:0.02:4];
% x = repmat(x',1 ,2);

v = randn(2, 1);
v = v ./ norm(v);

y = (x*v).^2;
% y = max(x*v, -0.5 * x*v);

% plot(x(:, 1), x(:, 2), 'ro')
% hold on
% 
% plot(v)

[xtemp, indx] = sort(x*v);
plot(xtemp, y(indx))
hold on
plot(xtemp, y(indx), 'ro')
%% test algorithms
options.MaxIter = 1000; 
% options.Display = 'iter'; 
% options.Display = 'final'; 
options.Display = 'off';
options.Algorithm = 'interior-point-convex'; 
options.TolCon = 1e-6; 

options.gb = 100;
options.fb = 100;

options.iter = 200;
options.gamma = 10;
options.ninit = 10;
options.vtrue = v;
options.gradient = 2;
options.seed = 100;

[alpha, zpos, vlearn, isconvex, vdiff, ydiff] = convex_single_index(x, y, options);


% %% plot figure
figure
[xtest, indx] = sort(x*vlearn);
[ytest, g] = pwl_evaluation(alpha, zpos, 1, xtest);
plot(xtest, ytest, 'LineWidth', 10)
hold on
plot(xtest, y(indx), 'g*')

figure 
plot(vdiff)

figure
plot(ydiff)