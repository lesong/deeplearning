
function [alpha, zpos, v, vdiff, ydiff] = monotonic_single_index(x, y, options)

% INPUT
% param x, y: data points to be interpolated
%             x is n-by-d vector
%             y is n-by-1 vector
% param options: options parameter for the algorithm
%                optimization method
%                options.method -- 1: QP solver
%                                  2: PAV algorithm
%                optimization options for QP solver
%                options.TolCon -- tolerance for QP solver
%                options.MaxIter -- maximum iteration for QP solver
%                options.Algorithm -- algorithm for QP solver
%                options.Display
%                options.fb -- bound for convex function value
%                options.gb -- bound for gradient value

%                optimization options for out loop
%                options.iter -- number of iterations


% param options.vture: for debug

% OUTPUT
% param alpha: alpha = [yest, g] 2n-by-1 vector, first n elements are the
% value of the function, following n elements are the gradients
% param zpos: sorted x*v, n-by-1 vector
% param v: projection direction

[n, d] = size(x);

% random initialize the projection direction
% 
randn('seed', options.seed)
v = randn(d, 1);
% v = zeros(d, 1);

vdiff = zeros(options.iter, 1);
ydiff = zeros(options.iter, 1);
obj = zeros(options.iter, 1);

xv = x * options.vtrue;
[xtemp, idx] = sort(xv);

for i = 1: options.iter
     
    % update h
    xproj = x * v;
    if options.method == 1
        % QP solver
        [alpha, zpos, optval] = qp_monotonic_fitting(xproj, y, options);
    else
        % PAV algorithm
        [alpha, zpos, optval] = lsqisotonic_modified(xproj, y, ones(length(y), 1));
    end
    
    plot(zpos, alpha(1:n), 'm')
    hold on
    [~, indx] = sort(xproj);
    iord(indx) = 1:n;
    yy = alpha(iord);
    plot(zpos, alpha(1:n), 'go')
    plot(xtemp, y(idx));
    plot(xtemp, y(idx), 'r*')

    for j = 1: n
       line([xv(j), xproj(j)], [y(j), yy(j)], 'Color', 'c')
    end
    drawnow

    
    % update v
    hout = alpha(iord);
    % ftt = monotonic_evaluation(alpha, zpos, v, x);
    % norm(hout - ftt)
    % plot(xtemp, ftt(idx), 'kd')
    % stepsize = options.gamma / (options.ninit + sqrt(i));
    stepsize = 1;
    if options.gradient == 1
        v = v +  stepsize / n * x' * (y - hout);
    else
        gtemp = alpha(n+1:end);
        v = v +  stepsize / n * x' * ((y - hout) .* gtemp(iord));
    end
    
    
    vdiff(i) = (v'/norm(v) * options.vtrue./norm(options.vtrue));
    ydiff(i) = norm(hout - y, 2);
    obj(i) = optval;
    
    if i>1 && abs(obj(i) - obj(i-1)) < 1e-12
        return
    end
    
    if i ~= options.iter
        clf()
    end
end
