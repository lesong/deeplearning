
function [f, g] = pwl_evaluation(alpha, zpos, v, x)

% INPUT
% param alpha: 2n-by-1 vector. First half saves the points values, second
% half saves the corresponding gradients
% param v: d-by-1 vector, projection direction.
% param x: m-by-d vector. The m points need to be evaluated.

% OUTPUT
% param f: m-by-1 vector, the corresponding PWL function values
% param g: m-by-1 vector, the corresponding PWL gradients


z = x * v;
z = z';


[n, ~] = size(alpha);
h = alpha(1:n/2 - 1);
grad = alpha(n/2+1 : n-1);
pair_diff  = bsxfun(@minus, z, zpos(1:n/2-1));

gtemp = repmat(grad', length(z), 1);
htemp = repmat(h', length(z), 1);

ftemp = htemp + gtemp .* pair_diff';
[f, indx] = max(ftemp, [], 2);
g = grad(indx);
