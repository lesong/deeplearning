\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{Definitions}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}



\newcommand{\Le}[1]{{\color{red}{\bf\sf [note: #1]}}}
\newcommand{\Santosh}[1]{{\color{pink}{\bf\sf [note: #1]}}}
\newcommand{\Bo}[1]{{\color{blue}{\bf\sf [note: #1]}}}

\title{Deep learning with SGD}


\begin{document} 

\maketitle


\section{Special property of stationary points}
For a mini-batch $Z = (X, Y) \in (\RR^{d \times m}, \RR^m)$ of $m$ data points, consider the loss function
\begin{align}
L(w, Z) = \frac{1}{m}\sum_i l(y_i, f(x_i;w))
\end{align}
where $l(\cdot,\cdot)$ is a convex loss.

A stationary point at the batch satisfies
\begin{align}
\nabla_w L(w, Z) = \frac{1}{m} \sum_i l^{\prime}(y_i, f(x_i;w)) \nabla_w f(x_i; w) = 0
\end{align}

If the derivatives $\cbr{\nabla_w f(x_i; w) }_i$ are linearly independent, then we have
\begin{align}
l^{\prime}(y_i, f(x_i;w)) = 0, \text{ for all } i
\end{align}

Since $l(\cdot, \cdot)$ is a convex loss, we know $L(w, Z)$ is minimized at the stationary point $w$.

%\subsection{Comments}
%Let $A$ be the derivative matrix and $x$ the loss vector. 
%Let $f$ be $L$ fully-connected layers with activation $\sigma$:
%$$
%	f(x_i; w) = w^{L+1} h^{L}, ~~h^i = \sigma(W^i h^{i-1}) (1\le i \le L), h^0 = x.
%$$
%Then the derivative on $W^i_{j:}$, the $j$-th row of the parameter matrix at $i$-th layer, can be computed as
%$$
% \frac{\partial f}{\partial W^i_{j:}} = [W^{L+1} \sigma'(W^L h^{L-1})^\top W^{L} \cdots W^{i+1} \sigma'(W^i h^{i-1})^\top]_j  h^{i-1} = c^i h^{i-1}
%$$
%where $[v]_i$ is the $i$-th entry in the vector $v$.
%Assume $c^(x) \neq 0$, then a sufficient condition for that $A$ is full rank is that for at least one layer $i$, 
%the set $\{h^{i}(x_j): 1\le j \le m\}$ is independent.
%
%Two worries: The idea seems to be counter intuitive.
%1. What if the optimum solution has non zero loss? When full rank, the loss of the stationary points should be 0, but this corresponds to the case when the number of  the parameters  larger than the number of data points, ie, the overfitting case. We can argue about regularization over the parameters but things become more trickier.
%We can consider under what conditions of $A$, $Ax=0$ and $\|x\|_\infty \le 1$ implies small $\|x\|_2$, where $x$ is the loss vector.
%
%2. What if there are two identical data points? Then the matrix is not full rank. But one more identical data points should not change the intuition: we can just remove the identical point and repeat the argument. 
%
%Now consider the more general case: there are similar data points. This is in constrast to the full rank requirement which says that data points scatter around the representation space. But as we argue above, two identical data points cause no problem, so similar data points should not cause big problem. There seems to be some tradeoff we can manipulate.


\section{The structure of the derivative matrix}
Suppose we use the least squares loss $l(y, f) = \frac{1}{2}\rbr{y - f}^2$, and the function is defined as 
\begin{align}
f(x; w) = \sum_{i=1}^n \sigma(w_i^\top x)
\end{align}

The derivative wrt $w_i$ is thus
\begin{align}
\frac{\partial L}{\partial w_i} &= \frac{1}{m} \sum_{j=1}^m \rbr{f(x_j; w) - y_j}\sigma^{\prime}(w_i^\top x_j) x_j  \nonumber \\
&= \frac{1}{m} \sbr{\sigma^{\prime}(w_i^\top x_1) x_1, \cdots, \sigma^{\prime}(w_i^\top x_m) x_m} \sbr{f(x_1) - y_1, \cdots, f(x_m) - y_m}^\top
\end{align}

Denote the residue matrix as 
\begin{align}
r = \sbr{\begin{array}{c}
f(x_1) - y_1 \\ \cdots \\ f(x_m) - y_m
\end{array}}
\end{align}

Denote the derivative matrix for the function as
\begin{align}
D &= \sbr{\begin{array}{ccc}
\sigma^{\prime}(w_1^\top x_1) x_1 & \cdots & \sigma^{\prime}(w_1^\top x_m) x_m \\
\cdots & \cdots  & \cdots \\
\sigma^{\prime}(w_i^\top x_1) x_1 & \cdots & \sigma^{\prime}(w_i^\top x_m) x_m \\
\cdots & \cdots  & \cdots \\
\sigma^{\prime}(w_n^\top x_1) x_1 & \cdots & \sigma^{\prime}(w_n^\top x_m) x_m
\end{array}}  \nonumber\\
&= \sbr{\begin{array}{ccc}
\sigma^{\prime}(w_1^\top x_1) & \cdots & \sigma^{\prime}(w_1^\top x_m)\\
\cdots & \cdots  & \cdots \\
\sigma^{\prime}(w_n^\top x_1) & \cdots & \sigma^{\prime}(w_n^\top x_m) 
\end{array}} \odot
\sbr{\begin{array}{ccc}
x_1 & \cdots & x_m
\end{array}}
\end{align}
where $\odot$ is the Khatri-Rao product. We call the first part consisting $\sigma^{\prime}$ as the $\Sigma$ matrix and the second part as $X$.

The goal is prove the matrix $D \in \RR^{(d \times n) \times m}$ is full rank and have a good (small) condition number. There are two conflicting factors.
\begin{itemize}
\item We have $m$ to be small such that $D$ is more likely to be full rank.
\item We have $m$ to be large enough, such that according to concentration inequalities, we have the gradient of this batch is not far away from the expected gradient.
\end{itemize}




\section{Update for the derivative matrix}
Let's focus on $\Sigma$ matrix since only it contains $w$. The update for $w$ is
\begin{align}
w_{t+1} &= w_t - \eta_t \nabla_w L(w_t) \nonumber\\
&= w_t - \eta_t \rbr{\Sigma \odot X} r
\end{align}

We use Taylor expansion around $w_t$ to get
\begin{align}
\sigma^{\prime}\rbr{w_{t+1}^\top x} = \sigma^{\prime}\rbr{w_{t}^\top x} - \eta_t \sigma^{\prime\prime}\rbr{w_{t}^\top x} \sbr{\rbr{\Sigma \odot X} r}^\top x
\end{align}

Denote the $j$-th column of the change matrix as
\begin{align}
\Delta_j = \sigma^{\prime\prime}\rbr{w_{t}^\top x_j} \sbr{\rbr{\Sigma \odot X} r}^\top x_j
\end{align}

After one iteration, $\Sigma$ matrix changes to 
\begin{align}
\Sigma  - \eta_t \Delta
\end{align}

\section{Analyzing SGD in the old way}
We need to assume the function's gradient is $B$-Lipschitz, which implies
\begin{align}
L(w_1) \le L(w_2) + \inner{\nabla L(w_2)}{w_1 - w_2} + \frac{B}{2}\nbr{w_1 - w_2}^2
\end{align}
where $L(w) = \EE_Z\sbr{ L(w, Z)}$.

The SGD update is
\begin{align}
w_{t+1} = w_t - \eta_t G(w_t, z_t)
\end{align}
where $G(w_t, z_t)$ is the unbiased stochastic gradient, \ie, $\EE\sbr{G(w_t, z_t)}=\nabla_w L(w_t)$, with maximum variance $\sigma$.

The convergence rate for SGD is
\begin{align}
\EE_{z\sbr{N}}\sbr{\nbr{\nabla_w \EE_Z\sbr{L(w_N, Z)}}^2} \le
\frac{B\rbr{D_L^2 + \sigma^2\sum_{t=1}^N \eta_t^2}}{\sum_{t=1}^N 2\eta_t - B \eta_t^2}
\end{align}

That is, after $O(1/\epsilon^2)$ iterations, one gets an $\epsilon$ error on the squared norm of the expected gradient. With high probability,
\begin{align}
\nbr{\nabla_w \EE_Z\sbr{L(w_N, Z)}}^2 \le \epsilon .
\end{align}

If we use a large enough sample to approximate the expectation, due to concentration inequality, we get with high probability
\begin{align}
\nbr{ \frac{1}{m}\sum_i  \nabla_w l(y_i, f(x_i;w))  }^2 \le \epsilon
\end{align}

Denote $\kappa$ the condition number of the matrix $\cbr{\nabla_w f(x_i; w) }_i$, we then have
\begin{align}
\nbr{l^{\prime}}^2 \le \epsilon \kappa^2
\end{align}
where $\sbr{l^{\prime}}_i = l^{\prime}(y_i, f(x_i;w))$.

\textbf{To do: } Need to show $\kappa$ is small.



\section{Analyze directly}
We want to look at a new loss function averaged over $m$ test points
\begin{align}
R(w) = \frac{1}{m} \sum_{i=1}^m l(y_i, f(x_i;w))
\end{align}

Suppose it is $B$-Lipschitz, then we have
\begin{align}
R(w_{t+1}) &\le R(w_t) + \inner{\nabla_w R(w_t)}{w_{t+1} - w_t} + \frac{B}{2}\nbr{w_{t+1} - w_t}^2 \nonumber\\
&=  R(w_t) - \eta_t \inner{\nabla_w R(w_t)}{G(w_t, Z_t)} + \frac{B\eta_t^2}{2}\nbr{G(w_t, Z_t)}^2
\end{align}

The stochastic gradient can be decomposed as
\begin{align}
G(w_t, Z_t) = \nabla R(w_t) + (\nabla L(w_t) - \nabla R(w_t)) + \delta_t
\end{align}
where $\EE\sbr{\delta_t} = 0$ and $\EE\sbr{\delta_t^2} \le \sigma^2$.

Expanding the stochastic gradient, we get
\begin{align}
R_{t+1} &\le  R_t - \eta_t \cbr{\nbr{\nabla R_t}^2 + \inner{\nabla R_t}{\nabla L_t - \nabla R_t} + \inner{\nabla R_t}{\delta_t}} \nonumber\\
 &+ \frac{B\eta_t^2}{2}\cbr{\nbr{\nabla R_t}^2 + \nbr{\nabla L_t - \nabla R_t}^2 + \nbr{\delta_t}^2 + 2\inner{\nabla R_t}{\nabla L_t - \nabla R_t}
 +2 \inner{\nabla R_t}{\delta_t} + 2\inner{\nabla L_t - \nabla R_t}{\delta_t}} \nonumber\\
&\le R_t - \rbr{\eta_t - \frac{B\eta_t^2}{2}}\nbr{\nabla R_t}^2
-\rbr{\eta_t - B \eta_t^2}\cbr{\inner{\nabla R_t}{\nabla L_t - \nabla R_t} + \inner{\nabla R_t}{\delta_t}}  \nonumber\\
&+ \frac{B\eta_t^2}{2}\cbr{\nbr{\nabla L_t - \nabla R_t}^2 + \nbr{\delta_t}^2 + 2\inner{\nabla L_t - \nabla R_t}{\delta_t}}
\end{align}

Summing over $t=1$ to $t=N$, and rearranging terms leads to
\begin{align}
\sum_{t=1}^{N} \rbr{\eta_t - \frac{B\eta_t^2}{2}}\nbr{\nabla R_t}^2 &\le R_1 - R_{N+1}
-\sum_{t=1}^N \rbr{\eta_t - B \eta_t^2}\cbr{\inner{\nabla R_t}{\nabla L_t - \nabla R_t} + \inner{\nabla R_t}{\delta_t}}  \nonumber\\
&+ \sum_{t=1}^N  \frac{B\eta_t^2}{2}\cbr{\nbr{\nabla L_t - \nabla R_t}^2 + \nbr{\delta_t}^2 + 2\inner{\nabla L_t - \nabla R_t}{\delta_t}}
\end{align}

Taking expectations of $Z[N]$ and note that $\EE\sbr{\delta_t^2} \le \sigma^2$, and
\begin{align}
\EE\sbr{\inner{\nabla R_t}{\delta_t}} &= 0 \nonumber\\
\EE\sbr{\inner{\nabla L_t - \nabla R_t}{\delta_t}} &= 0
\end{align}

This simplifies to
\begin{align}
\sum_{t=1}^{N} \rbr{\eta_t - \frac{B\eta_t^2}{2}}\EE\nbr{\nabla R_t}^2 
&\le R_1 - \hat{R}_* + \sum_{t=1}^N  \frac{B\eta_t^2}{2} \sigma^2  \nonumber\\
&-\sum_{t=1}^N \rbr{\eta_t - B \eta_t^2} \EE\inner{\nabla R_t}{\nabla L_t - \nabla R_t}
+ \sum_{t=1}^N  \frac{B\eta_t^2}{2} \EE\nbr{\nabla L_t - \nabla R_t}^2
\end{align}

As a result, we need to bound $\EE\inner{\nabla R_t}{\nabla L_t - \nabla R_t}$ and $ \EE\nbr{\nabla L_t - \nabla R_t}^2$.


%%%%%%%%%%%%%%%%%%%% Reference %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliographystyle{unsrt}
\bibliography{../Bibfile/bibfile}

\end{document} 
